cmake_minimum_required(VERSION 3.10)
project(arc)

set(CMAKE_CXX_STANDARD 17)

add_subdirectory(engine)
add_subdirectory(editor)
