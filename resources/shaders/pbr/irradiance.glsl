#ifdef _VERTEX_
layout(location = 0) in vec3 vertex;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 uv;

out vec3 vo_position;

uniform mat4 v_view_matrix;
uniform mat4 v_projection_matrix;

void main()
{
    vo_position = vertex;

    gl_Position = v_view_matrix * vec4(vertex, 1.0);
}

#endif

#ifdef _FRAGMENT_
in vec3 vo_position;

out vec4 fo_color;

const float PI = 3.14159265359;

layout(binding = 0) uniform samplerCube f_environment;

void main()
{
    vec3 N = normalize(vo_position);
    vec3 irradiance = vec3(0.0);

    vec3 up = vec3(0.0, 1.0, 0.0);
    vec3 right = cross(up, N);
    up = cross(N, right);

    float sample_delta = 0.025;
    float nr_samples = 0.0;

    for (float phi = 0.0; phi < 2.0 * PI; phi += sample_delta) {
        for (float theta = 0.0; theta < 0.5 * PI; theta += sample_delta) {
            vec3 tangent_sample = vec3(sin(theta) * cos(phi), sin(theta) * sin(phi), cos(theta));
            vec3 sample_vec = tangent_sample.x * right + tangent_sample.y * up + tangent_sample.z * N;

            irradiance += texture(f_environment, sample_vec).rgb * cos(theta) * sin(theta);
            ++nr_samples;
        }
    }

    irradiance = PI * irradiance * (1.0 / float(nr_samples));

    fo_color = vec4(irradiance, 1.0);
}

#endif
