#ifdef _VERTEX_
layout(location = 0) in vec3 vertex;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 uv;

out vec2 vo_texture;

void main()
{
    vo_texture = uv;

    gl_Position = vec4(vertex, 1.0);
}

#endif

#ifdef _FRAGMENT_
in vec2 vo_texture;

out vec4 fo_color;

layout(binding = 0) uniform sampler2D f_attachment0;

void main()
{
    vec2 texel_size = 1.0 / vec2(textureSize(f_attachment0, 0));
    float result = 0.0;

    for (int x = -2; x < 2; x++) {
        for (int y = -2; y < 2; y++) {
            vec2 offset = vec2(x, y) * texel_size;
            result += texture(f_attachment0, vo_texture + offset).r;
        }
    }

    fo_color = vec4(vec3(result / (4 * 4)), 1.0);
}

#endif
