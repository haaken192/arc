#ifdef _VERTEX_
layout(location = 0) in vec2 position;
layout(location = 1) in vec2 uv;
layout(location = 2) in vec4 color;

out vec2 vo_texture;
out vec4 vo_color;

uniform mat4 v_projection_matrix;

void main()
{
	vo_texture = uv;
	vo_color = color;

	gl_Position = v_projection_matrix * vec4(position, 0, 1);
}

#endif

#ifdef _FRAGMENT_
in vec2 vo_texture;
in vec4 vo_color;

out vec4 fo_color;

layout(binding = 0) uniform sampler2D f_attachment0;

void main()
{
	fo_color = vo_color * texture(f_attachment0, vo_texture).r;
}

#endif
