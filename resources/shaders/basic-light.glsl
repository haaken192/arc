#ifdef _VERTEX_
layout(location = 0) in vec3 vertex;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 uv;

out vec3 vo_position;
out vec3 vo_normal;
out vec2 vo_texture;

uniform mat4 v_mvp_matrix;
uniform mat4 v_projection_matrix;
uniform mat4 v_view_matrix;
uniform mat4 v_model_matrix;
uniform mat3 v_normal_matrix;

void main()
{
    vo_position = vec3(v_model_matrix * vec4(vertex, 1.0));
    vo_normal = normal;
    vo_texture = uv;

    gl_Position = v_projection_matrix * v_view_matrix * v_model_matrix * vec4(vertex, 1.0);
}

#endif

#ifdef _FRAGMENT_
in vec3 vo_position;
in vec3 vo_normal;
in vec2 vo_texture;

out vec4 fo_color;

uniform vec3 f_camera;

layout(binding = 0) uniform sampler2D f_attachment0;

void main()
{
    // Temporary
    vec3 lightColor = vec3(1.0, 0.98, 0.957);
    vec3 lightPos = vec3(0.0, 5.0, 1.0);
    vec3 albedo = vec3(1.0, 1.0, 1.0);

    // ambient
    float ambientStrength = 0.2;
    vec3 ambient = ambientStrength * lightColor;

    // diffuse
    vec3 norm = normalize(vo_normal);
    vec3 lightDir = normalize(lightPos - vo_position);
    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = diff * lightColor;

    // specular
    float specularStrength = 0.5;
    vec3 viewDir = normalize(f_camera - vo_position);
    vec3 reflectDir = reflect(-lightDir, norm);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), 32);
    vec3 specular = specularStrength * spec * lightColor;

//    vec3 color = vo_normal * 0.5 + 0.5;
    vec3 color = (ambient + diffuse + specular) * albedo;

    fo_color = vec4(color, 1.0);
}

#endif
