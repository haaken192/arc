cmake_minimum_required(VERSION 3.10)
project(editor)

set(CMAKE_CXX_STANDARD 17)

set(HEADER_FILES
        include/editor/constants.h
        include/editor/scene/load.h
        include/editor/scene/editor.h)

set(SOURCE_FILES
        src/scene/editor.cpp
        src/scene/load.cpp
        src/main.cpp)

include_directories("${CMAKE_CURRENT_SOURCE_DIR}/include")
include_directories("${CMAKE_CURRENT_SOURCE_DIR}/../engine/include")
include_directories("${CMAKE_CURRENT_SOURCE_DIR}/../engine/lib")

set(ARC_LIBRARIES_DIR "" CACHE STRING "")

if(MINGW)
    link_directories("${ARC_LIBRARIES_DIR}/mingw-w64")
elseif(MSW)
    link_directories("${ARC_LIBRARIES_DIR}/msw")
elseif(UNIX)
    link_directories("${ARC_LIBRARIES_DIR}/linux")
endif()

include_directories("${ARC_LIBRARIES_DIR}/include")
add_executable(arc-editor ${HEADER_FILES} ${SOURCE_FILES})

if(MINGW)
    target_link_libraries(arc-editor arc opengl32 glfw3)
elseif(MSW)
    target_link_libraries(arc-editor arc opengl32 glfw3)
elseif(UNIX)
    target_link_libraries(arc-editor arc GL glfw3 X11 Xi Xxf86vm Xrandr Xcursor Xinerama pthread dl)
endif()
