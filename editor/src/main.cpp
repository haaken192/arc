//
// Created by tswanson on 10/1/18.
//
#include <memory>
#include <stack>

#include <arc/core/app.h>
#include <arc/core/asset.h>
#include <arc/core/log.h>
#include <arc/core/scene.h>
#include <arc/gfx/renderer.h>

#include <imgui/imgui.h>

#include <editor/constants.h>
#include <editor/scene/editor.h>
#include <editor/scene/load.h>

int main() {

    auto app = std::make_unique<arc::App>(arc::App::Properties{
        .name = "ArcEdit",
        .company_name = "HaakenLabs",
        .version = "0.0.1",
        .pre_setup_func = nullptr,
    });

    arc::set_log_level(arc::trace);
    LOG_INFO("Welcome to the Arc Editor");

    arc::SceneManager::add(std::make_shared<editor::SceneLoad>());
    arc::SceneManager::add(std::make_shared<editor::SceneEditor>());
    arc::SceneManager::push(editor::SCENE_LOAD_NAME);

    app->run();
}
