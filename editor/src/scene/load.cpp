/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <editor/scene/load.h>

#include <imgui/imgui.h>

#include <arc/core/asset.h>

#include <editor/constants.h>

using namespace editor;

static void load_func() {
    arc::AssetManager::add_manifest("resources/assets.json");
    arc::AssetManager::add_manifest("resources/cubemaps.json");
}

static void display_func() {
    if (arc::AssetManager::done()) {
        arc::SceneManager::purge_push(SCENE_EDITOR_NAME);
    }
    arc::AssetManager::process();

    ImVec2 window_pos = ImVec2(ImGui::GetIO().DisplaySize.x / 2, ImGui::GetIO().DisplaySize.y / 2);
    ImVec2 window_pos_pivot = ImVec2(0.5f, 0.5f);

    ImGui::SetNextWindowPos(window_pos, ImGuiCond_Always, window_pos_pivot);
    ImGui::SetNextWindowBgAlpha(0.3f);
    ImGui::SetNextWindowSize(ImVec2(512,0));

    if (ImGui::Begin("", nullptr, ImGuiWindowFlags_NoMove| ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoFocusOnAppearing | ImGuiWindowFlags_NoNav)) {
        ImGui::Text("Loading Assets");
        ImGui::Separator();
        ImGui::Spacing();
        ImGui::ProgressBar(arc::AssetManager::get_metrics().progress());
        ImGui::Text("Loading %s", arc::AssetManager::get_metrics().current_asset_.c_str());
    }
    ImGui::End();
}

SceneLoad::SceneLoad()
: Scene(SCENE_LOAD_NAME) {
    load_func_ = load_func;
    display_func_ = display_func;
}
