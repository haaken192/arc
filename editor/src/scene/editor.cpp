/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <editor/scene/editor.h>

#include <imgui/imgui.h>

#include <arc/core/app.h>
#include <arc/core/asset.h>
#include <arc/core/log.h>
#include <arc/scene/camera.h>
#include <arc/scene/control_orbit.h>
#include <arc/core/transform.h>
#include <arc/core/entity.h>
#include <arc/gfx/material.h>
#include <arc/scene/mesh_renderer.h>
#include <arc/gfx/mesh.h>
#include <arc/gfx/shader.h>
#include <arc/gfx/skybox.h>
#include <arc/gfx/texture.h>

#include <editor/constants.h>

using namespace editor;

static bool show_menu_bar_ = true;
static bool show_window_scene_ = false;
static bool show_window_assets_ = false;
static bool show_window_renderer_ = false;
static bool show_window_debug_ = false;
static bool show_window_logs_ = false;
static bool show_window_metrics_ = false;
static bool show_window_demo_ = false;
static bool show_window_about_ = false;

static void gui_main_menu() {
    if (ImGui::BeginMainMenuBar()) {
        if (ImGui::BeginMenu("File")) {
            if (ImGui::MenuItem("Quit", "Alt+F4")) {
                arc::App::quit();
            }
            ImGui::EndMenu();
        }
        if (ImGui::BeginMenu("Entity")) {
            arc::SceneManager::get_active()->gui_menu_entity();
            ImGui::EndMenu();
        }
        if (ImGui::BeginMenu("Window"))
        {
            if (ImGui::MenuItem("Scene", "Ctrl+1")) {
                show_window_scene_ = true;
            }
            if (ImGui::MenuItem("Assets", "Ctrl+2")) {
                show_window_assets_ = true;
            }
            if (ImGui::MenuItem("Renderer", "Ctrl+3")) {
                show_window_renderer_ = true;
            }
            if (ImGui::MenuItem("Debug", "Ctrl+4")) {
                show_window_debug_ = true;
            }
            ImGui::Separator();
            if (ImGui::MenuItem("Logs", "Ctrl+~")) {
                show_window_logs_ = true;
            }
            ImGui::EndMenu();
        }
        if (ImGui::BeginMenu("Help"))
        {
            if (ImGui::MenuItem("Metrics")) {
                show_window_metrics_ = true;
            }
            if (ImGui::MenuItem("ImGui Demo Window")) {
                show_window_demo_ = true;
            }
            ImGui::Separator();
            if (ImGui::MenuItem("About")) {
                show_window_about_ = true;
            }
            ImGui::EndMenu();
        }
    }
    ImGui::EndMainMenuBar();
}

static void gui_window_scene(bool* _p_open) {
    if (!ImGui::Begin("Scene", _p_open)) {
        ImGui::End();
        return;
    }

    arc::SceneManager::get_active()->gui_inspector_scene();

    ImGui::End();
}

static void gui_window_hierarchy(bool* _p_open) {
    if (!ImGui::Begin("Hierarchy", _p_open)) {
        ImGui::End();
        return;
    }

    ImGui::Text("Test field:");
    ImGui::Columns(3);
    ImGui::Separator();
    ImGui::Text("One"); ImGui::NextColumn();
    ImGui::Text("Two"); ImGui::NextColumn();
    ImGui::Text("Three"); ImGui::NextColumn();
    ImGui::Separator();
    ImGui::Text("Alpha"); ImGui::NextColumn();
    ImGui::Text("Beta"); ImGui::NextColumn();
    ImGui::Text("Gamma"); ImGui::NextColumn();
    ImGui::Columns(1);
    ImGui::End();
}

static void gui_window_assets(bool* _p_open) {
    if (!ImGui::Begin("Assets", _p_open)) {
        ImGui::End();
        return;
    }

    arc::AssetManager::show_gui();

    ImGui::End();
}

static void gui_window_renderer(bool* _p_open) {
    if (!ImGui::Begin("Renderer", _p_open)) {
        ImGui::End();
        return;
    }

    arc::SceneManager::get_active()->gui_panel_renderer(nullptr);

    ImGui::End();
}

static void gui_window_debug(bool* _p_open) {
    if (!ImGui::Begin("Debug", _p_open)) {
        ImGui::End();
        return;
    }

    ImGui::End();
}

static void gui_window_logs(bool* _p_open) {
    if (!ImGui::Begin("Logs", _p_open)) {
        ImGui::End();
        return;
    }

    arc::Log::show_gui();

    ImGui::End();
}

static void gui_window_about(bool* _p_open) {
    ImGui::Begin("About", _p_open, ImGuiWindowFlags_AlwaysAutoResize);
    ImGui::Text("%s", arc::App::get_name().c_str());
    ImGui::Separator();
    ImGui::Text("Company: %s", arc::App::get_company_name().c_str());
    ImGui::Text("Version: %s", arc::App::get_version().c_str());
    ImGui::Text("Dear ImGui: %s", ImGui::GetVersion());
    ImGui::Separator();
    ImGui::Text("ArcEngine is licensed under the MIT License.");
    ImGui::End();
}

static void show_gui() {
    /* Main Menu */
    if (show_menu_bar_) gui_main_menu();

    /* Windows */
    if (show_window_scene_) gui_window_scene(&show_window_scene_);
    if (show_window_assets_) gui_window_assets(&show_window_assets_);
    if (show_window_renderer_) gui_window_renderer(&show_window_renderer_);
    if (show_window_debug_) gui_window_debug(&show_window_debug_);
    if (show_window_logs_) gui_window_logs(&show_window_logs_);
    if (show_window_about_) gui_window_about(&show_window_about_);
    if (show_window_metrics_) ImGui::ShowMetricsWindow(&show_window_metrics_);
    if (show_window_demo_) ImGui::ShowDemoWindow(&show_window_demo_);
}

static void display_func() {
    show_gui();
}

static void load_func() {
    auto s = arc::SceneManager::get_active();

    s->set_skybox(std::make_shared<arc::Skybox>(arc::AssetManager::get_asset_ptr<arc::TextureCubemap>("bell_park_pier_8k.hdr")));

    auto c = arc::create_camera("Camera");
    c->get_transform()->set_position(glm::vec3(0, 0, 4), true);
    c->get_component<arc::Camera>()->update_matrices();
    c->add_component(std::make_shared<arc::ControlOrbit>());

    s->add_entity(c, 0);
    s->add_entity(arc::create_3d_object("Orb", "orb.obj"), 0);
}

SceneEditor::SceneEditor()
: Scene(SCENE_EDITOR_NAME) {
    load_func_ = load_func;
    display_func_ = display_func;
}

