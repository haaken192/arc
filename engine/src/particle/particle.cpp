/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <arc/particle/particle.h>

#include <imgui/imgui.h>

#include <arc/core/asset.h>
#include <arc/core/transform.h>
#include <arc/core/time.h>
#include <arc/gfx/shader.h>
#include <arc/gfx/texture.h>

using namespace arc;

#define WORKGROUP_SIZE 128

ParticleSystem::ParticleSystem()
: time_start_(0.f)
, in_offset_(0)
, out_offset_(10000)
, playing_(true)
, paused_(false)
, buffer_flip_(false) {
    class_name = "ParticleSystem";
    name = "ParticleSystem";

    core = std::make_unique<particle::Core>(10000);
    emission = std::make_unique<particle::Emission>();

    render_shader_ = AssetManager::get_asset_ptr<Shader>("particle/render");
    sprite_ = AssetManager::get_asset_ptr<Texture2D>("particle.png");
}

void ParticleSystem::gui_inspector() {
    ImGui::Text("Max Particles: %d", core->max_particles_);
    ImGui::Text("Particles: %d", core->get_particle_count());
    ImGui::Text("Alive particles: %d", core->alive_);
    ImGui::Text("Dead particles: %d", core->dead_);
    ImGui::Text("Emit particles: %d", core->emit_);
    ImGui::Text("Emission rate: %f", emission->rate);
}

void ParticleSystem::simulate() {
    auto delta_time = (float)(Time::get_delta() * core->playback_speed);

    core->lifecycle_shader_->bind();
    core->buffer_->bind();

    core->lifecycle_shader_->set_uniform("u_offset_in", in_offset_);
    core->lifecycle_shader_->set_uniform("u_offset_out", out_offset_);
    core->lifecycle_shader_->set_uniform("u_max_particles", core->max_particles_);
    core->lifecycle_shader_->set_uniform("u_delta_time", delta_time);
    core->lifecycle_shader_->set_uniform("u_start_color", core->start_color.as_vec4());
    core->lifecycle_shader_->set_uniform("u_angular_velocity_3d", glm::vec3(0));
    core->lifecycle_shader_->set_uniform("u_rotation", glm::vec3(0));
    core->lifecycle_shader_->set_uniform("u_position", glm::vec3(0));
    core->lifecycle_shader_->set_uniform("u_random_seed", (uint32_t)(Time::get_frame()));
    core->lifecycle_shader_->set_uniform("u_angular_velocity", 10.f);
    core->lifecycle_shader_->set_uniform("u_start_lifetime", core->start_lifetime);
    core->lifecycle_shader_->set_uniform("u_start_size", core->start_size);
    core->lifecycle_shader_->set_uniform("u_velocity", core->start_speed);

    core->buffer_->reset_index(core->alive_, core->dead_);

    if (core->alive_ > 0) {
        core->lifecycle_shader_->set_subroutine(ShaderComponentCompute, "task_lifetime");
        core->lifecycle_shader_->set_uniform("u_invocations", core->alive_);

        glDispatchCompute((core->alive_/WORKGROUP_SIZE)+1, 1, 1);
        glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
    }

    auto emit_now = (uint32_t)glm::ceil(emission->rate * delta_time);

    if (emit_now > 0 && core->dead_ > 0) {
        if (emit_now > core->dead_) {
            emit_now = core->dead_;
        }

        core->lifecycle_shader_->set_subroutine(ShaderComponentCompute, "task_emit");
        core->lifecycle_shader_->set_uniform("u_invocations", emit_now);

        glDispatchCompute((emit_now/WORKGROUP_SIZE)+1, 1, 1);
        glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
    }

    core->sync_counts();


    // Simulate phase
    if (core->alive_ > 0) {
        core->simulate_shader_->bind();
        core->simulate_shader_->set_uniform("u_invocations", core->alive_);
        core->simulate_shader_->set_uniform("u_offset_out", out_offset_);
        core->simulate_shader_->set_uniform("u_delta_time", delta_time);
        core->simulate_shader_->set_uniform("u_attractors", false);

        glDispatchCompute((core->alive_/WORKGROUP_SIZE)+1, 1, 1);
        glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
        core->simulate_shader_->unbind();
    }

    swap_buffers();
}

void ParticleSystem::draw(const Renderable* _renderable) {
    simulate();

    render_shader_->bind();
    core->bind_buffer();

    glDisable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE);

    render_shader_->set_uniform("v_model_matrix", get_transform()->get_active_matrix());
    render_shader_->set_uniform("v_view_matrix", _renderable->get_view_matrix());
    render_shader_->set_uniform("v_projection_matrix", _renderable->get_projection_matrix());
    render_shader_->set_uniform("v_offset", in_offset_);

    sprite_->activate(0);
    glDrawArrays(GL_POINTS, 0, core->alive_);

    core->unbind_buffer();
    render_shader_->bind();

    glEnable(GL_DEPTH_TEST);
    glDisable(GL_BLEND);
}

void ParticleSystem::draw_shader(const Renderable* _renderable, const Shader* _shader) {
    draw(_renderable);
    _shader->bind();
}

bool ParticleSystem::is_deferrable() const {
    return false;
}

void ParticleSystem::stop() {
    playing_ = false;
    paused_ = false;
}

void ParticleSystem::play() {
    playing_ = true;
    paused_ = false;
}

void ParticleSystem::pause() {
    playing_ = false;
    paused_ = true;
}

void ParticleSystem::swap_buffers() {
    buffer_flip_ = !buffer_flip_;

    in_offset_ = 0;
    out_offset_ = core->max_particles_;

    if (buffer_flip_) {
        in_offset_ = core->max_particles_;
        out_offset_ = 0;
    }
}
