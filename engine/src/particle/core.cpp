/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <arc/particle/core.h>

#include <gl3w/GL/gl3w.h>

#include <arc/core/asset.h>

using namespace arc::particle;

Core::Core(uint32_t _max_particles)
: start_color(Color::white)
, duration(0)
, start_delay(0)
, start_lifetime(5.f)
, start_speed(10.f)
, start_size(1.f)
, playback_speed(1.f)
, random_seed(1)
, looping(true)
, alive_(0)
, dead_(_max_particles)
, emit_(0)
, max_particles_(_max_particles) {
    buffer_ = std::make_unique<Buffer>(_max_particles);

    lifecycle_shader_ = AssetManager::get_asset_ptr<Shader>("particle/lifecycle");
    simulate_shader_ = AssetManager::get_asset_ptr<Shader>("particle/simulate");
}

void Core::set_max_particles(uint32_t _max_particles) {
    max_particles_ = _max_particles;
    dead_ = _max_particles;
    alive_ = 0;

    buffer_->set_size(_max_particles);
}

void Core::sync_counts() {
    buffer_->bind();

    glBindBuffer(GL_SHADER_STORAGE_BUFFER, buffer_->id_index_);
    auto p = glMapBuffer(GL_SHADER_STORAGE_BUFFER, GL_READ_ONLY);

    uint32_t index_data[3];
    memcpy(index_data, p, sizeof(index_data));

    alive_ = index_data[0];
    dead_ = index_data[1];
    emit_ = index_data[2];

    glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);

    buffer_->unbind();
}

uint32_t Core::get_max_particles() const {
    return max_particles_;
}

uint32_t Core::get_particle_count() const {
    return alive_;
}

void Core::bind_buffer() const {
    buffer_->bind();
}

void Core::unbind_buffer() const {
    buffer_->unbind();
}
