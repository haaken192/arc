/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <arc/particle/buffer.h>

#include <gl3w/GL/gl3w.h>

using namespace arc::particle;

#define PARTICLE_SIZE 80

Buffer::Buffer(uint32_t _size)
: size_(_size) {
    class_name = "particle::Buffer";
    name = "particle::Buffer";

    glGenVertexArrays(1, &vao_);
    glGenBuffers(1, &id_particle_);
    glGenBuffers(1, &id_alive_);
    glGenBuffers(1, &id_dead_);
    glGenBuffers(1, &id_index_);
    glGenBuffers(1, &id_attractors_);

    bind();

    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, id_particle_);
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, id_particle_);
    glBufferData(GL_SHADER_STORAGE_BUFFER, int(size_)*PARTICLE_SIZE, nullptr, GL_DYNAMIC_DRAW);

    // Alive List
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, id_alive_);
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, id_alive_);
    glBufferData(GL_SHADER_STORAGE_BUFFER, int(size_*2)*4, nullptr, GL_DYNAMIC_DRAW);

    // Dead List
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, id_dead_);
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, id_dead_);
    glBufferData(GL_SHADER_STORAGE_BUFFER, int(size_)*4, nullptr, GL_DYNAMIC_DRAW);

    // Indices
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 3, id_index_);
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, id_index_);
    glBufferData(GL_SHADER_STORAGE_BUFFER, 12, nullptr, GL_DYNAMIC_DRAW);

    // Attractors
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 4, id_attractors_);
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, id_attractors_);
    glBufferData(GL_SHADER_STORAGE_BUFFER, 48, nullptr, GL_DYNAMIC_DRAW);

    unbind();
}

Buffer::~Buffer() {
    glDeleteBuffers(1, &id_particle_);
    glDeleteBuffers(1, &id_alive_);
    glDeleteBuffers(1, &id_dead_);
    glDeleteBuffers(1, &id_index_);
    glDeleteBuffers(1, &id_attractors_);
    glDeleteVertexArrays(1, &vao_);
}

void Buffer::bind() {
    glBindVertexArray(vao_);
}

void Buffer::unbind() {
    glBindVertexArray(0);
}

void Buffer::reserve() {
    bind();

    // Particle Pool
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, id_particle_);
    glBufferData(GL_SHADER_STORAGE_BUFFER, int(size_)*PARTICLE_SIZE, nullptr, GL_DYNAMIC_DRAW);

    // Alive List
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, id_alive_);
    glBufferData(GL_SHADER_STORAGE_BUFFER, int(size_*2)*4, nullptr, GL_DYNAMIC_DRAW);

    // Dead List
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, id_dead_);
    std::vector<uint32_t> dead(size_, 0);
    for (uint32_t i = 0; i < dead.size(); i++) {
        dead[i] = size_ - i - 1;
    }
    glBufferData(GL_SHADER_STORAGE_BUFFER, int(size_)*4, dead.data(), GL_DYNAMIC_DRAW);

    // Indices
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, id_index_);
    uint32_t index_data[3] = {0, size_, 0};
    glBufferData(GL_SHADER_STORAGE_BUFFER, 12, index_data, GL_DYNAMIC_DRAW);

    unbind();
}

void Buffer::set_size(uint32_t _size) {
    size_ = _size;

    reserve();
}

void Buffer::reset_index(uint32_t _alive, uint32_t  _dead) {
    uint32_t data[3] = {_alive, _dead, 0};

    bind();
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, id_index_);
    glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(data), data, GL_DYNAMIC_DRAW);
    unbind();
}
