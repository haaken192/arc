/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <arc/gfx/image.h>

#include <chrono>
#include <vector>

#ifndef STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_IMPLEMENTATION
#endif
#include <stb/stb_image.h>
#include <glm/gtc/matrix_transform.hpp>

#include <arc/core/log.h>
#include <arc/core/resource.h>
#include <arc/gfx/framebuffer.h>
#include <arc/gfx/mesh.h>
#include <arc/gfx/shader.h>
#include <arc/gfx/texture.h>

using namespace arc;

texture_ptr arc::load_image(const resource_ptr& _resource) {
    auto start = std::chrono::system_clock::now();

    stbi_set_flip_vertically_on_load(true);
    int32_t width = 0;
    int32_t height = 0;
    int32_t nr_components = 0;

    if (stbi_is_hdr_from_memory(_resource->data_as<stbi_uc>(), (int)_resource->size())) {
        auto ptr = stbi_loadf_from_memory(_resource->data_as<stbi_uc>(), (int)_resource->size(), &width, &height, &nr_components, 0);
        if (!ptr) {
            printf("Failed to load HDR image\n");
            return nullptr;
        }

        auto t = std::make_shared<Texture2D>(TextureFormatRGB16, glm::ivec2(width, height), false);
        t->upload_data(ptr);
        t->set_name(_resource->base());

        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double, std::milli> diff = end-start;

        LOG_DEBUG("Loaded HDR image in {} ms. {}x{} ({})", diff.count(), width, height, nr_components);
        stbi_image_free(ptr);

        return t;
    } else {
        auto data = stbi_load_from_memory(_resource->data_as<stbi_uc>(), (int)_resource->size(), &width, &height, &nr_components, 0);
        if (!data) {
            printf("Failed to load standard image\n");
            return nullptr;
        }

        TextureFormat fmt;

        switch (nr_components) {
            case 1:
                fmt = TextureFormatR8;
                break;
            case 2:
                fmt = TextureFormatRG8;
                break;
            case 3:
                fmt = TextureFormatRGB8;
                break;
            case 4:
                fmt = TextureFormatRGBA8;
                break;
            default:
                throw std::invalid_argument("Invalid number of channels");
        }

        auto t = std::make_shared<Texture2D>(fmt, glm::ivec2(width, height), false);
        t->upload_data(data);
        t->set_name(_resource->base());

        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double, std::milli> diff = end-start;

        LOG_DEBUG("Loaded standard image in {} ms. {}x{} ({})", diff.count(), width, height, nr_components);
        stbi_image_free(data);

        return t;
    }
}

std::shared_ptr<TextureCubemap> arc::load_cubemap(const resource_ptr& _resource) {
    auto source = load_image(_resource);
    if (!source) {
        throw std::runtime_error("Failed to load source image");
    }

    auto cubemap = std::make_shared<TextureCubemap>(source->get_format(), source->get_size().y/2);
    cubemap->set_name(_resource->base());

    auto fbo = std::make_shared<Framebuffer>(cubemap->get_size());
    auto mesh = AssetManager::get_asset<Mesh>("cube.obj");
    auto shader = AssetManager::get_asset<Shader>("utils/cubeconv");

    fbo->bind();
    mesh->bind();
    shader->bind();

    source->activate(0);

    glm::mat4 rotation_matrices[6] = {
            glm::lookAt(glm::vec3(0), glm::vec3(-1, 0, 0), glm::vec3(0, -1, 0)),
            glm::lookAt(glm::vec3(0), glm::vec3(1, 0, 0), glm::vec3(0, -1, 0)),
            glm::lookAt(glm::vec3(0), glm::vec3(0, -1, 0), glm::vec3(0, 0, -1)),
            glm::lookAt(glm::vec3(0), glm::vec3(0, 1, 0), glm::vec3(0, 0, 1)),
            glm::lookAt(glm::vec3(0), glm::vec3(0, 0, 1), glm::vec3(0, -1, 0)),
            glm::lookAt(glm::vec3(0), glm::vec3(0, 0, -1), glm::vec3(0, -1, 0)),
    };

    shader->set_uniform("v_projection_matrix", glm::perspective(glm::half_pi<float>(), 1.0f, 0.1f, 2.0f));

    for (uint32_t i = 0; i < 6; i++) {
        shader->set_uniform("v_view_matrix", rotation_matrices[i]);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, cubemap->get_reference(), 0);
        fbo->clear_buffers();
        mesh->draw();
    }

    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, 0, 0);

    mesh->unbind();
    shader->unbind();
    fbo->unbind();

    return cubemap;
}