/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <arc/gfx/ssao.h>

#include <random>

#include <imgui/imgui.h>

#include <arc/gfx/attachment.h>
#include <arc/gfx/framebuffer.h>
#include <arc/gfx/mesh.h>
#include <arc/gfx/renderer.h>
#include <arc/gfx/shader.h>
#include <arc/gfx/texture.h>
#include <arc/gfx/gui.h>
#include <arc/core/asset.h>
#include <arc/core/window.h>

using namespace arc;

static inline float lerp(float a, float b, float f)
{
    return a + f * (b - a);
}

SSAO::SSAO()
: radius_(0.5f)
, bias_(0.025f)
, kernel_samples_(32)
, enabled_(false) {
    auto size = Window::get_resolution();

    fbo_ = std::make_shared<Framebuffer>(size);
    noise_texture_ = std::make_shared<Texture2D>(TextureFormatRGB32, glm::ivec2(4, 4));

    ssao_attachment_ = std::make_shared<AttachmentTexture2D>(TextureFormatR8, fbo_->get_size());
    ssao_blur_attachment_ = std::make_shared<AttachmentTexture2D>(TextureFormatR8, fbo_->get_size());

    ssao_shader_ = AssetManager::get_asset_ptr<Shader>("effects/ssao");
    blur_shader_ = AssetManager::get_asset_ptr<Shader>("effects/ssao_blur");

    fbo_->set_attachment(GL_COLOR_ATTACHMENT0, ssao_attachment_);
    if (!fbo_->validate()) {
        throw std::runtime_error("SSAO primary attachment failed to validate");
    }

    fbo_->set_attachment(GL_COLOR_ATTACHMENT0, ssao_blur_attachment_);
    if (!fbo_->validate()) {
        throw std::runtime_error("SSAO blur attachment failed to validate");
    }


    mesh_ = create_mesh_quad();

    generate_textures();
}

void SSAO::compute(const texture_ptr& _gbuffer_0, const texture_ptr& _gbuffer_1, const texture_ptr& _depth, const Renderable* _renderable) {
    mesh_->bind();

    // SSAO pass
    ssao_shader_->bind();

    _gbuffer_0->activate(0);
    _gbuffer_1->activate(1);
    _depth->activate(2);
    noise_texture_->activate(3);

    fbo_->bind();
    fbo_->set_attachment(GL_COLOR_ATTACHMENT0, ssao_attachment_);
    fbo_->update_attachments();
    fbo_->clear_buffers();

    ssao_shader_->set_uniform("f_kernel_size", kernel_samples_);
    ssao_shader_->set_uniform("f_radius", radius_);
    ssao_shader_->set_uniform("f_bias", bias_);
    ssao_shader_->set_uniform("f_resolution", glm::vec2(fbo_->get_size()));
    ssao_shader_->set_uniform("f_view_matrix", _renderable->get_view_matrix());
    ssao_shader_->set_uniform("f_projection_matrix", _renderable->get_projection_matrix());
    for (uint32_t i = 0; i < ssao_kernel_.size(); i++) {
        ssao_shader_->set_uniform("f_samples[" + std::to_string(i) + "]", ssao_kernel_[i]);
    }

    mesh_->draw();

    // Blur pass
    blur_shader_->bind();
    fbo_->set_attachment(GL_COLOR_ATTACHMENT0, ssao_blur_attachment_);
    ssao_attachment_->get_object()->activate(0);
    fbo_->update_attachments();
    fbo_->clear_buffers();

    mesh_->draw();
    blur_shader_->unbind();
    fbo_->unbind();
    mesh_->unbind();
}

void SSAO::gui_inspector() {
    ImGui::Checkbox("Enabled", &enabled_);
    ImGui::InputFloat("Radius", &radius_, 0.01f, 1.0f);
    ImGui::InputFloat("Bias", &bias_, 0.001f, 1.0f);

    if (ImGui::CollapsingHeader("SSAO Texture")) {
        gui_texture(ssao_attachment_->get_object());
    }
    if (ImGui::CollapsingHeader("SSAO Blur Texture")) {
        gui_texture(ssao_blur_attachment_->get_object());
    }
    if (ImGui::CollapsingHeader("Noise Texture")) {
        gui_texture(noise_texture_);
    }
}

void SSAO::generate_textures() {
    // SSAO kernel
    std::uniform_real_distribution<GLfloat> random_floats(0.0f, 1.0f);
    std::default_random_engine generator;

    for (uint8_t i = 0; i < kernel_samples_; i++) {
        glm::vec3 sample(random_floats(generator) * 2.0 - 1.0, random_floats(generator) * 2.0 - 1.0, random_floats(generator));
        sample = glm::normalize(sample);
        sample *= random_floats(generator);
        float scale = float(i) / 64.0f;

        scale = lerp(0.1f, 1.0f, scale * scale);
        sample *= scale;
        ssao_kernel_.push_back(sample);
    }

    // Noise texture
    std::vector<glm::vec3> ssao_noise;
    for (uint32_t i = 0; i < 16; i++) {
        glm::vec3 noise(random_floats(generator) * 2.0 - 1.0, random_floats(generator) * 2.0 - 1.0, 0.0f);
        ssao_noise.push_back(noise);
    }
    noise_texture_->upload_data(ssao_noise.data());
    noise_texture_->bind();
    noise_texture_->set_wrap(GL_REPEAT, GL_REPEAT, GL_REPEAT);
    noise_texture_->set_filter(GL_NEAREST, GL_NEAREST);
    noise_texture_->unbind();
}
