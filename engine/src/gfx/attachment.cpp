#include <utility>

/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <arc/gfx/attachment.h>

#include <arc/gfx/renderbuffer.h>
#include <arc/gfx/texture.h>

using namespace arc;


AttachmentRenderbuffer::AttachmentRenderbuffer(std::shared_ptr<Renderbuffer> _renderbuffer)
: Attachment(std::move(_renderbuffer)) {}

AttachmentRenderbuffer::AttachmentRenderbuffer(GLenum _format, glm::ivec2 _size)
: Attachment(std::make_shared<Renderbuffer>(_format, _size)) {}

void AttachmentRenderbuffer::attach(GLenum _location) {
    object_->attach(_location);
}

void AttachmentRenderbuffer::set_size(glm::ivec2 _size) {
    object_->set_size(_size);
}

AttachmentTexture2D::AttachmentTexture2D(std::shared_ptr<Texture2D> _texture)
: Attachment(std::move(_texture)) {}

AttachmentTexture2D::AttachmentTexture2D(TextureFormat _format, glm::ivec2 _size)
: Attachment(std::make_shared<Texture2D>(_format, _size)) {}

void AttachmentTexture2D::attach(GLenum _location) {
    glFramebufferTexture2D(GL_FRAMEBUFFER, _location, GL_TEXTURE_2D, object_->get_reference(), 0);
}

void AttachmentTexture2D::set_size(glm::ivec2 _size) {
    object_->set_size(_size);
}
