/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <arc/gfx/renderbuffer.h>

using namespace arc;

Renderbuffer::Renderbuffer(GLenum _format, glm::ivec2 _size)
: format_(_format)
, size_(_size) {
    class_name = "Renderbuffer";
    name = "Renderbuffer";

    glGenRenderbuffers(1, &reference_);

    glBindRenderbuffer(GL_RENDERBUFFER, reference_);
    glRenderbufferStorage(GL_RENDERBUFFER, format_, size_.x, size_.y);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);
}

Renderbuffer::~Renderbuffer() {
    glDeleteRenderbuffers(1, &reference_);
}

void Renderbuffer::attach(GLenum _location) {
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, _location, GL_RENDERBUFFER, reference_);
}

void Renderbuffer::set_size(glm::ivec2 _size) {
    if (_size.x > 0 && _size.y > 0) {
        size_ = _size;
        glBindRenderbuffer(GL_RENDERBUFFER, reference_);
        glRenderbufferStorage(GL_RENDERBUFFER, format_, size_.x, size_.y);
        glBindRenderbuffer(GL_RENDERBUFFER, 0);
    }
}

uint32_t Renderbuffer::get_reference() const {
    return reference_;
}

glm::ivec2 Renderbuffer::get_size() const {
    return size_;
}
