/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <arc/gfx/mesh.h>

#include <chrono>
#include <fstream>
#include <vector>
#include <string>
#include <unordered_map>

#include <gl3w/GL/gl3w.h>
#include <imgui/imgui.h>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/hash.hpp>
#define TINYOBJLOADER_IMPLEMENTATION
#include <tinyobjloader/tiny_obj_loader.h>

#include <arc/core/log.h>
#include <arc/core/resource.h>

using namespace arc;

namespace std {
    template<>
    struct hash<arc::Vertex> {
        size_t operator()(arc::Vertex const &vertex) const {
            return ((hash<glm::vec3>()(vertex.position) ^ (hash<glm::vec3>()(vertex.normal) << 1)) >> 1) ^
                   (hash<glm::vec2>()(vertex.uv) << 1);
        }
    };
}

Mesh::Mesh()
: vao(0)
, vbo(0)
, ibo_(0)
, indices_(0) {
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    glGenBuffers(1, &vbo);
    glGenBuffers(1, &ibo_);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo_);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3) * 2 + sizeof(glm::vec2), nullptr);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3) * 2 + sizeof(glm::vec2), reinterpret_cast<void*>(sizeof(glm::vec3)));
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(glm::vec3) * 2 + sizeof(glm::vec2), reinterpret_cast<void*>(2 * sizeof(glm::vec3)));

    glBindVertexArray(0);
}

Mesh::~Mesh() {
    glDeleteBuffers(1, &ibo_);
    glDeleteBuffers(1, &vbo);
    glDeleteVertexArrays(1, &vao);
}

void Mesh::set_positions(std::vector<glm::vec3> _positions) {
    positions = std::move(_positions);
}

void Mesh::set_normals(std::vector<glm::vec3> _normals) {
    normals = std::move(_normals);
}

void Mesh::set_uvs(std::vector<glm::vec2> _uvs) {
    uvs = std::move(_uvs);
}

void Mesh::upload() {
    if (positions.empty() || normals.empty() || uvs.empty()) {
        LOG_ERROR("{} (vao: {}) has invalid geometry definition.", name.c_str(), vao);
        return;
    }
    if (positions.size() != normals.size() || normals.size() != uvs.size()) {
        LOG_ERROR("{} (vao: {}) has invalid geometry definition.", name.c_str(), vao);
        return;
    }

    std::vector<Vertex> data = {};
    data.reserve(positions.size());

    for (size_t i = 0; i < positions.size(); i++) {
        data.push_back(Vertex{positions[i], normals[i], uvs[i]});
    }

    bind();
    verts = (GLsizei)data.size();
    indices_ = 0;
    glBufferData(GL_ARRAY_BUFFER, data.size() * sizeof(Vertex), &data[0], GL_STATIC_DRAW);
    unbind();
}

void Mesh::bind() const {
    glBindVertexArray(vao);
}

void Mesh::unbind() const {
    glBindVertexArray(0);
}

void Mesh::draw() const {
    if (indices_ > 0) {
        glDrawElements(GL_TRIANGLES, int32_t(indices_), GL_UNSIGNED_INT, nullptr);
    } else if (verts > 0) {
        glDrawArrays(GL_TRIANGLES, 0, verts);
    }
}

void Mesh::clear() {
    positions.clear();
    normals.clear();
    uvs.clear();
}

std::vector<glm::vec3> Mesh::get_positions() const {
    return positions;
}

std::vector<glm::vec3> Mesh::get_normals() const {
    return normals;
}

std::vector<glm::vec2> Mesh::get_uvs() const {
    return uvs;
}

bool Mesh::load(const resource_ptr& _resource) {
    auto start = std::chrono::system_clock::now();
    LOG_DEBUG("Loading model: {}", _resource->location().c_str());

    set_name(_resource->base());

    tinyobj::attrib_t attrib;
    std::vector<tinyobj::shape_t> shapes;
    std::vector<tinyobj::material_t> materials;
    std::string err;
    std::vector<Vertex> vertices;
    std::vector<uint32_t> indices;
    std::unordered_map<Vertex, uint32_t> uniqueVertices = {};

    auto stream = std::istringstream(_resource->string());

    if (!tinyobj::LoadObj(&attrib, &shapes, &materials, &err, &stream)) {
        LOG_ERROR("failed to load model");
        return false;
    }

    for (const auto& shape : shapes) {
        for (const auto& index : shape.mesh.indices) {
            Vertex vertex = {};

            vertex.position = {
                attrib.vertices[3 * index.vertex_index + 0],
                attrib.vertices[3 * index.vertex_index + 1],
                attrib.vertices[3 * index.vertex_index + 2],
            };
            if (!attrib.normals.empty()) {
                vertex.normal = {
                        attrib.normals[3 * index.normal_index + 0],
                        attrib.normals[3 * index.normal_index + 1],
                        attrib.normals[3 * index.normal_index + 2],
                };
            }

            if (!attrib.texcoords.empty()) {
                vertex.uv = {
                        attrib.texcoords[2 * index.texcoord_index + 0],
                        attrib.texcoords[2 * index.texcoord_index + 1],
                };
            }

            if (!uniqueVertices.count(vertex)) {
                uniqueVertices[vertex] = static_cast<uint32_t>(vertices.size());
                vertices.push_back(vertex);
            }

            indices.push_back(uniqueVertices[vertex]);
        }
    }

    bind();
    indices_ = static_cast<int32_t>(indices.size());
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), vertices.data(), GL_STATIC_DRAW);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(uint32_t), indices.data(), GL_STATIC_DRAW);
    unbind();

    auto end = std::chrono::system_clock::now();
    std::chrono::duration<double, std::milli> diff = end-start;
    LOG_DEBUG("Loaded model in {} ms", diff.count());

    return true;
}

MeshHandler::MeshHandler()
: AssetHandler("mesh") {}

void MeshHandler::load(const resource_ptr& _resource) {
    auto mesh = std::make_shared<Mesh>();

    if (mesh->load(_resource)) {
        items_[mesh->get_name()] = mesh;
    }
}

void MeshHandler::gui_inspector() {
    for (auto const &c: items_) {
        ImGui::BulletText("%s", c.second->get_name().c_str());
    }
}

mesh_ptr arc::create_mesh_quad() {
    auto m = std::make_shared<Mesh>();

    m->set_positions(std::vector<glm::vec3>{
            {-1.0, 1.0, 0.0},
            {-1.0, -1.0, 0.0},
            {1.0, -1.0, 0.0},
            {-1.0, 1.0, 0.0},
            {1.0, -1.0, 0.0},
            {1.0, 1.0, 0.0},
    });
    m->set_normals(std::vector<glm::vec3>{
            {0.0, 0.0, 1.0},
            {0.0, 0.0, 1.0},
            {0.0, 0.0, 1.0},
            {0.0, 0.0, 1.0},
            {0.0, 0.0, 1.0},
            {0.0, 0.0, 1.0},
    });
    m->set_uvs(std::vector<glm::vec2>{
            {0.0, 1.0},
            {0.0, 0.0},
            {1.0, 0.0},
            {0.0, 1.0},
            {1.0, 0.0},
            {1.0, 1.0},
    });

    m->upload();

    return m;
}

mesh_ptr arc::create_mesh_quadback() {
    auto m = std::make_shared<Mesh>();

    m->set_positions(std::vector<glm::vec3>{
            {-1.0, 1.0, 1.0},
            {-1.0, -1.0, 1.0},
            {1.0, -1.0, 1.0},
            {-1.0, 1.0, 1.0},
            {1.0, -1.0, 1.0},
            {1.0, 1.0, 1.0},
    });
    m->set_normals(std::vector<glm::vec3>{
            {0.0, 0.0, 1.0},
            {0.0, 0.0, 1.0},
            {0.0, 0.0, 1.0},
            {0.0, 0.0, 1.0},
            {0.0, 0.0, 1.0},
            {0.0, 0.0, 1.0},
    });
    m->set_uvs(std::vector<glm::vec2>{
            {0.0, 1.0},
            {0.0, 0.0},
            {1.0, 0.0},
            {0.0, 1.0},
            {1.0, 0.0},
            {1.0, 1.0},
    });

    m->upload();

    return m;
}
