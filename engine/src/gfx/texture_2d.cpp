/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <arc/gfx/texture.h>

#include <imgui/imgui.h>

#include <arc/gfx/image.h>

using namespace arc;

Texture2D::Texture2D(TextureFormat _format, bool _resizable)
: Texture2D(_format, glm::ivec2(0), _resizable) {}

Texture2D::Texture2D(TextureFormat _format, glm::ivec2 _size, bool _resizable)
: Texture(GL_TEXTURE_2D, _format)
{
    class_name = "Texture2D";
    name = "Texture2D";
    size_ = _size;
    resizable_ = _resizable;

    bind();
    upload();
}

void Texture2D::upload_data(void* _data) {
    bind();
    glTexImage2D(type_, 0, internal_, size_.x, size_.y, 0, glformat_, storage_, _data);
    unbind();
}

void Texture2D::load(const resource_ptr& _resource) {

}

void Texture2D::gui_inspector() {
    ImGui::Text("Texture type: GL_TEXTURE_2D");
    ImGui::Text("Reference: %u", reference_);

    auto scaled_h = int32_t((float(size_.y) / float(size_.x)) * 256.f);

    ImGui::Image(
            (ImTextureID)(intptr_t)reference_,
            ImVec2(256, scaled_h),
            ImVec2(0,1), ImVec2(1,0),
            ImColor(255,255,255,255), ImColor(255,255,255,128));

}

TextureHandler::TextureHandler()
: AssetHandler("texture") {}

void TextureHandler::load(const resource_ptr& _resource) {
    if (auto t = std::dynamic_pointer_cast<Texture2D>(load_image(_resource))) {
        items_[t->get_name()] = t;
    }
}

void TextureHandler::gui_inspector() {
    for (auto const &c: items_) {
        if (ImGui::TreeNode(c.first.c_str())) {
            c.second->gui_inspector();
            ImGui::TreePop();
        }
    }
}
