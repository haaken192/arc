#include <utility>

/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <arc/gfx/material.h>

#include <glm/gtc/type_ptr.hpp>
#include <imgui/imgui.h>

#include <arc/gfx/texture.h>
#include <arc/gfx/gui.h>

using namespace arc;

Material::Material(shader_ptr _shader)
: shader_(std::move(_shader)) {
    albedo_ = std::make_shared<TextureColor>();
    metallic_ = std::make_shared<TextureColor>();
    roughness_ = std::make_shared<TextureColor>();

    textures_[7] = albedo_;
    textures_[8] = metallic_;
    textures_[10] = roughness_;
}

void Material::set_shader(shader_ptr _shader) {
    shader_ = std::move(_shader);
}

void Material::set_texture(texture_ptr _texture, MaterialTexture _id) {
    textures_[_id] = std::move(_texture);
}

void Material::set_texture(texture_ptr _texture, uint8_t _id) {
    if (_id < 32) {
        textures_[_id] = std::move(_texture);

    }
}

void Material::set_property(const std::string &_name, const ShaderProperty &_value) {
    shader_properties_[_name] = _value;
}

Shader* Material::get_shader() const {
    return shader_.get();
}

Texture* Material::get_texture(MaterialTexture _id) const {
    try {
        return textures_.at(static_cast<GLuint>(_id)).get();
    } catch (const std::out_of_range& _e) {
        return nullptr;
    }
}

Texture* Material::get_texture(uint8_t _id) const {
    try {
        return textures_.at(static_cast<GLuint>(_id)).get();
    } catch (const std::out_of_range& _e) {
        return nullptr;
    }
}

void Material::bind() {
    shader_->bind();
    for (auto const &pair : shader_properties_) {
        shader_->set_uniform(pair.first, pair.second);
    }
    for (auto const &pair : textures_) {
        pair.second->activate(pair.first);
    }
}

void Material::unbind() {
    shader_->unbind();
}

void Material::gui_inspector() {
    if (ImGui::TreeNode("Shader")) {
        shader_->gui_inspector();
        ImGui::TreePop();
    }

    if (ImGui::TreeNode("PBR Settings")) {
        auto albedo_value = albedo_->get_color();
        auto metallic_value = metallic_->get_color().x;
        auto roughness_value = roughness_->get_color().x;

        if (ImGui::ColorPicker4("Albedo", glm::value_ptr(albedo_value))) {
            albedo_->set_color(albedo_value);
        }

        ImGui::Image(
                (ImTextureID)(intptr_t)metallic_->get_reference(),
                ImVec2(16, 16),
                ImVec2(0,1), ImVec2(1,0),
                ImColor(255,255,255,255), ImColor(255,255,255,128));
        ImGui::SameLine();
        if (ImGui::SliderFloat("Metallic", &metallic_value, 0.f, 1.f)) {
            metallic_->set_color(glm::vec4(metallic_value));
        }
        ImGui::Image(
                (ImTextureID)(intptr_t)roughness_->get_reference(),
                ImVec2(16, 16),
                ImVec2(0,1), ImVec2(1,0),
                ImColor(255,255,255,255), ImColor(255,255,255,128));
        ImGui::SameLine();
        if (ImGui::SliderFloat("Roughness", &roughness_value, 0.f, 1.f)) {
            roughness_->set_color(glm::vec4(roughness_value));
        }
        ImGui::TreePop();
    }

    if (ImGui::TreeNode("Properties")) {
        ImGui::Columns(3, "shader_properties_table");
        ImGui::Separator();
        ImGui::Text("Name"); ImGui::NextColumn();
        ImGui::Text("Type"); ImGui::NextColumn();
        ImGui::Text("Value"); ImGui::NextColumn();
        ImGui::Separator();

        for (auto &pair : shader_properties_) {
            ImGui::Text("%s", pair.first.c_str()); ImGui::NextColumn();
            ImGui::Text("%s", boost::apply_visitor(shader_property_name_visitor(), pair.second)); ImGui::NextColumn();
            boost::apply_visitor(shader_property_gui_visitor(), pair.second); ImGui::NextColumn();
        }

        ImGui::Columns(1);
        ImGui::TreePop();
    }

    if (ImGui::TreeNode("Textures")) {
        ImGui::Columns(3, "textures_table");
        ImGui::Separator();
        ImGui::Text("ID"); ImGui::NextColumn();
        ImGui::Text("Name"); ImGui::NextColumn();
        ImGui::Text("Preview"); ImGui::NextColumn();
        ImGui::Separator();

        for (auto const &pair : textures_) {
            ImGui::Text("%d", pair.first); ImGui::NextColumn();
            ImGui::Text("%s", pair.second->get_name().c_str()); ImGui::NextColumn();
            gui_texture(pair.second, 96); ImGui::NextColumn();
        }
        ImGui::Columns(1);
    }
}

bool Material::supports_deferred() const {
    return shader_->supports_deferred();
}
