/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <arc/gfx/tonemap.h>

#include <imgui/imgui.h>

#include <arc/core/asset.h>
#include <arc/gfx/mesh.h>
#include <arc/gfx/shader.h>

using namespace arc;

Tonemap::Tonemap()
: exposure_(.25f)
, average_grey_(0.4f)
, white_(2.0f) {
    shader_ = AssetManager::get_asset_ptr<Shader>("effect/tonemapper");
    mesh_ = create_mesh_quad();
}

void Tonemap::gui_inspector() {
    ImGui::InputFloat("Exposure", &exposure_, 0.05f, 1.0f);
    ImGui::InputFloat("Average Grey", &average_grey_, 0.1f, 1.0f);
    ImGui::InputFloat("White", &white_, 0.1f, 1.0f);
}

void Tonemap::compute() {
    mesh_->bind();
    shader_->bind();
    shader_->set_subroutine(ShaderComponentFragment, "pass_basic");
    shader_->set_uniform("f_exposure", exposure_);
    shader_->set_uniform("f_average_grey", average_grey_);
    shader_->set_uniform("f_white", white_);
    mesh_->draw();
    shader_->unbind();
    mesh_->unbind();
}
