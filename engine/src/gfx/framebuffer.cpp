/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <arc/gfx/framebuffer.h>

#include <arc/core/log.h>
#include <arc/core/window.h>
#include <arc/gfx/attachment.h>

using namespace arc;

std::stack<framebuffer_ptr> Framebuffer::framebuffer_stack = std::stack<framebuffer_ptr>{};

Framebuffer::Framebuffer()
: Framebuffer(glm::ivec2(0)) {}

Framebuffer::Framebuffer(glm::ivec2 _size)
: size_(_size)
, bound_(false) {
    glGenFramebuffers(1, &reference_);
}

Framebuffer::~Framebuffer() {
    glDeleteFramebuffers(1, &reference_);
}

bool Framebuffer::allocate() {
    raw_bind();

    for (auto &c : attachments_) {
        c.second->set_size(size_);
        c.second->attach(c.first);
    }

    if (!draw_buffers_.empty()) {
        glDrawBuffers(int32_t(draw_buffers_.size()), draw_buffers_.data());
    }

    if (!validate()) {
        raw_unbind();
        return false;
    }

    raw_unbind();

    return true;
}

void Framebuffer::raw_bind() const {
    glBindFramebuffer(GL_FRAMEBUFFER, reference_);
    glViewport(0, 0, size_.x, size_.y);
}

void Framebuffer::raw_unbind() const {
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    auto res = Window::get_resolution();
    glViewport(0, 0, res.x, res.y);
}

void Framebuffer::bind() {
    push(shared_from_this());
}

void Framebuffer::unbind() {
    if (bound_) {
        pop();
    } else {
        raw_unbind();
        auto res = Window::get_resolution();
        glViewport(0, 0, res.x, res.y);
    }
}

void Framebuffer::update_attachments() {
    for (auto &c : attachments_) {
        c.second->attach(c.first);
    }

    if (!draw_buffers_.empty()) {
        glDrawBuffers(int32_t(draw_buffers_.size()), draw_buffers_.data());
    }
}

void Framebuffer::clear_buffers() const {
    clear_buffer_flags(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
}

void Framebuffer::clear_buffer_flags(uint32_t _flags) const {
    glClear(_flags);
}

void Framebuffer::bind_current() {
    if (auto fbo = current()) {
        fbo->raw_bind();
    } else {
        fbo->raw_unbind();
    }
}

framebuffer_ptr Framebuffer::current() {
    if (framebuffer_stack.empty()) {
        return nullptr;
    }

    return framebuffer_stack.top();
}

void Framebuffer::pop() {
    if (framebuffer_stack.empty()) {
        return;
    }

    auto top = framebuffer_stack.top().get();
    top->raw_unbind();
    top->bound_ = false;
    framebuffer_stack.pop();

    if (!framebuffer_stack.empty()) {
        auto next = framebuffer_stack.top().get();
        next->raw_bind();
        next->bound_ = true;
    } else {
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        auto res = Window::get_resolution();
        glViewport(0, 0, res.x, res.y);
    }
}

void Framebuffer::push(framebuffer_ptr fbo) {
    if (!framebuffer_stack.empty()) {
        auto f = framebuffer_stack.top().get();
        f->raw_unbind();
        f->bound_ = false;
    }

    fbo->raw_bind();
    fbo->bound_ = true;
    framebuffer_stack.push(std::move(fbo));
}

bool Framebuffer::validate() const {
    uint32_t status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if (status == GL_FRAMEBUFFER_COMPLETE) {
        return true;
    }

    switch (status) {
        case GL_FRAMEBUFFER_UNSUPPORTED:
            LOG_ERROR("Framebuffer {} error: unsupported framebuffer format", reference_);
            break;
        case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
            LOG_ERROR("Framebuffer {} error: missing attachment", reference_);
            break;
        case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
            LOG_ERROR("Framebuffer {} error: incomplete attachment", reference_);
            break;
        case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
            LOG_ERROR("Framebuffer {} error: missing draw buffer", reference_);
            break;
        case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:
            LOG_ERROR("Framebuffer {} error: missing read buffer", reference_);
            break;
        default:
            LOG_ERROR("Framebuffer {} error: unknown error: %X", reference_, status);
            break;
    }

    return false;
}

glm::ivec2 Framebuffer::get_size() const {
    return size_;
}

uint32_t Framebuffer::get_reference() const {
    return reference_;
}

bool Framebuffer::has_attachment(uint32_t _location) const {
    return attachments_.find(_location) != attachments_.cend();
}

void Framebuffer::set_size(glm::ivec2 _size) {
    if (_size.x && _size.y) {
        size_ = _size;

        if (!allocate()) {
            throw std::runtime_error("failed to realloc buffer");
        }
    }
}

void Framebuffer::set_draw_buffers(std::vector<uint32_t> _buffers) {
    draw_buffers_ = std::move(_buffers);
}

void Framebuffer::apply_draw_buffers(std::vector<uint32_t> _buffers) {
    set_draw_buffers(std::move(_buffers));

    if (draw_buffers_.empty()) {
        glDrawBuffers(1, nullptr);
    } else {
        glDrawBuffers(int32_t(draw_buffers_.size()), draw_buffers_.data());
    }
}

void Framebuffer::set_attachment(uint32_t _location, attachment_ptr _attachment) {
    if (_attachment) {
        attachments_[_location] = _attachment;
    }
}

void Framebuffer::remove_all_attachments() {
    attachments_.clear();
}

void Framebuffer::remove_attachment(uint32_t _location) {
    auto itr = attachments_.find(_location);
    if (itr != attachments_.end()) {
        attachments_.erase(itr);
    }
}

attachment_ptr Framebuffer::get_generic_attachment(uint32_t _location) const {
    auto itr = attachments_.find(_location);
    if (itr != attachments_.cend()) {
        return itr->second;
    }

    return nullptr;
}

AttachmentGeneric* Framebuffer::get_generic_attachment_p(uint32_t _location) const {
    auto itr = attachments_.find(_location);
    if (itr != attachments_.cend()) {
        return itr->second.get();
    }

    return nullptr;
}

void Framebuffer::blit_framebuffers(const framebuffer_ptr& _in, const framebuffer_ptr& _out, GLenum _location) {
    auto src = _in->get_reference();
    auto dst = uint32_t(0);

    auto src_size = _in->get_size();
    auto dst_size = Window::get_resolution();

    if (_out) {
        dst = _out->get_reference();
        dst_size = _out->get_size();
    }

    glBindFramebuffer(GL_READ_FRAMEBUFFER, src);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, dst);
    glReadBuffer(_location);
    glBlitFramebuffer(0, 0, src_size.x, src_size.y, 0, 0, dst_size.x, dst_size.y, GL_COLOR_BUFFER_BIT, GL_LINEAR);

    bind_current();
}
