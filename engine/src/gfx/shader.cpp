/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <arc/gfx/shader.h>

#include <glm/gtc/type_ptr.hpp>
#include <imgui/imgui.h>

#include <arc/core/log.h>
#include <arc/core/resource.h>
#include <arc/util/json.h>

using namespace arc;

Shader::Shader(const std::string &_name, bool _deferred)
: reference(0)
, deferred_(_deferred) {}

Shader::~Shader() {
    destory_shader_parts(reference, components);
}

void Shader::bind() const {
    glUseProgram(reference);
}

void Shader::unbind() const {
    glUseProgram(0);
}

void Shader::set_subroutine(const ShaderComponent &_component_type, const std::string &_subroutine_name) const {
    const GLuint index = glGetSubroutineIndex(reference, _component_type, _subroutine_name.c_str());
    glUniformSubroutinesuiv(_component_type, 1, &index);
}

uint32_t Shader::get_reference() const {
    return reference;
}

void Shader::set_uniform(const std::string &_uniform_name, const ShaderProperty &_value) const
{
    boost::apply_visitor(shader_property_visitor(reference, _uniform_name.c_str()), _value);
}

void Shader::destroy_component(const GLuint &_program_id, const GLuint &_component_id) {
    glDetachShader(_program_id, _component_id);
    glDeleteShader(_component_id);
}


bool Shader::validate_component(const GLuint &_component_id) {
    int32_t status = 0;
    glGetShaderiv(_component_id, GL_COMPILE_STATUS, &status);

    if (status == GL_FALSE) {
        int32_t len = 0;
        glGetShaderiv(_component_id, GL_INFO_LOG_LENGTH, &len);

        auto buffer = std::vector<char>{};
        buffer.reserve(static_cast<size_t>(len));

        glGetShaderInfoLog(_component_id, len, nullptr, &buffer[0]);
        LOG_ERROR("shader {} compilation failed: {}", _component_id, buffer.data());
        return false;
    }

    return true;
}

bool Shader::validate_program(const GLuint &_program_id) {
    int32_t status = 0;
    glGetProgramiv(_program_id, GL_LINK_STATUS, &status);

    if (status == GL_FALSE) {
        int32_t len = 0;
        glGetProgramiv(_program_id, GL_INFO_LOG_LENGTH, &len);

        auto buffer = std::vector<char>{};
        buffer.reserve(static_cast<size_t>(len));

        glGetProgramInfoLog(_program_id, len, nullptr, &buffer[0]);
        LOG_ERROR("shader program {} link failed: {}", _program_id, buffer.data());
        return false;
    }

    return true;
}


void Shader::add_data(const std::vector<char> &_data) {
    data.insert(data.end(), _data.begin(), _data.end());
}

void Shader::add_data(const std::string &_data) {
    data.append(_data);
}

void Shader::clear_data() {
    data.clear();
}

bool Shader::compile() {
    std::map<ShaderComponent, GLuint> new_components;
    GLuint new_reference = 0;

    if (data.empty()) {
        LOG_ERROR("Cannot compile empty shader");
        return false;
    }

    new_reference = glCreateProgram();

    if (contains_shader_type(ShaderComponentVertex, data)) {
        auto id = load_component(new_reference, ShaderComponentVertex, data);
        if (!id) {
            destory_shader_parts(new_reference, new_components);
            return false;
        }
        new_components[ShaderComponentVertex] = id;
    }
    if (contains_shader_type(ShaderComponentGeometry, data)) {
        auto id = load_component(new_reference, ShaderComponentGeometry, data);
        if (!id) {
            destory_shader_parts(new_reference, new_components);
            return false;
        }
        new_components[ShaderComponentGeometry] = id;
    }
    if (contains_shader_type(ShaderComponentFragment, data)) {
        auto id = load_component(new_reference, ShaderComponentFragment, data);
        if (!id) {
            destory_shader_parts(new_reference, new_components);
            return false;
        }
        new_components[ShaderComponentFragment] = id;
    }
    if (contains_shader_type(ShaderComponentCompute, data)) {
        auto id = load_component(new_reference, ShaderComponentCompute, data);
        if (!id) {
            destory_shader_parts(new_reference, new_components);
            return false;
        }
        new_components[ShaderComponentCompute] = id;
    }
    if (contains_shader_type(ShaderComponentTessControl, data)) {
        auto id = load_component(new_reference, ShaderComponentTessControl, data);
        if (!id) {
            destory_shader_parts(new_reference, new_components);
            return false;
        }
        new_components[ShaderComponentTessControl] = id;
    }
    if (contains_shader_type(ShaderComponentTessEvaluation, data)) {
        auto id = load_component(new_reference, ShaderComponentTessEvaluation, data);
        if (!id) {
            destory_shader_parts(new_reference, new_components);
            return false;
        }
        new_components[ShaderComponentTessEvaluation] = id;
    }

    glLinkProgram(new_reference);

    if (!validate_program(new_reference)) {
        destory_shader_parts(new_reference, new_components);
        return false;
    }

    if (reference) {
        destory_shader_parts(reference, components);
    }

    reference = new_reference;
    components = new_components;

    LOG_DEBUG("Compiled and linked shader program %u", reference);

    return true;
}

bool Shader::recompile() {
    if (resource_) {

        resource_->reload();

        auto metadata = load_json(resource_->bytes());
        auto files = get_json_value<std::vector<std::string>>(metadata, "files");
        std::vector<char> data;

        if (files.empty()) {
            LOG_ERROR("shader has no source files");
            return false;
        }

        for (auto const &c : files) {
            auto src = Resource(resource_->dir_prefix() + "/" + c).bytes();
            data.insert(data.end(), src.begin(), src.end());
        }

        clear_data();
        add_data(data);
        if (!compile()) {
            LOG_ERROR("shader failed to compile");
            return false;
        }

        return true;
    }

    return false;
}

bool Shader::contains_shader_type(ShaderComponent _type, const std::string &_data) {
    switch (_type) {
        case ShaderComponentVertex:
            return _data.find("#ifdef _VERTEX_") != std::string::npos;
        case ShaderComponentGeometry:
            return _data.find("#ifdef _GEOMETRY_") != std::string::npos;
        case ShaderComponentFragment:
            return _data.find("#ifdef _FRAGMENT_") != std::string::npos;
        case ShaderComponentCompute:
            return _data.find("#ifdef _COMPUTE_") != std::string::npos;
        case ShaderComponentTessControl:
            return _data.find("#ifdef _TESSCONTROL_") != std::string::npos;
        case ShaderComponentTessEvaluation:
            return _data.find("#ifdef _TESSEVAL_") != std::string::npos;
    }

    return false;
}

GLuint Shader::load_component(const GLuint &_program_id, ShaderComponent _type, const std::string &_data) {
    std::string src = "#version 430\n";

    switch (_type) {
        case ShaderComponentVertex:
            src.append("#define _VERTEX_\n");
            break;
        case ShaderComponentGeometry:
            src.append("#define _GEOMETRY_\n");
            break;
        case ShaderComponentFragment:
            src.append("#define _FRAGMENT_\n");
            break;
        case ShaderComponentCompute:
            src.append("#define _COMPUTE_\n");
            break;
        case ShaderComponentTessControl:
            src.append("#define _TESSCONTROL_\n");
            break;
        case ShaderComponentTessEvaluation:
            src.append("#define _TESSEVAL_\n");
            break;
    }

    src.append(_data);

    const char *data_ptr = src.c_str();
    auto src_len = static_cast<int32_t>(src.length());

    GLuint component_id = glCreateShader(_type);
    glShaderSource(component_id, 1, &data_ptr, &src_len);
    glCompileShader(component_id);

    if (!validate_component(component_id)) {

        return 0;
    }

    glAttachShader(_program_id, component_id);

    return component_id;
}

bool Shader::supports_deferred() const {
    return deferred_;
}

void Shader::destory_shader_parts(const GLuint &_program_id, std::map<ShaderComponent, GLuint> &_components) {
    if (_program_id) {
        for (auto &c : _components) {
            destroy_component(_program_id, c.second);
        }

        glDeleteProgram(_program_id);
    }
}

bool Shader::load(const resource_ptr& _resource) {
    auto metadata = load_json(_resource->bytes());
    auto files = get_json_value<std::vector<std::string>>(metadata, "files");
    std::vector<char> data;

    deferred_ = get_json_value<bool>(metadata, "deferred", false);

    auto name =  get_json_value<std::string>(metadata, "name");
    if (std::string(name).empty()) {
        LOG_ERROR("shader name cannot be empty");
        return false;
    }
    set_name(name);

    if (files.empty()) {
        LOG_ERROR("shader has no source files");
        return false;
    }

    for (auto const &c : files) {
        auto src = Resource(_resource->dir_prefix() + "/" + c).bytes();
        data.insert(data.end(), src.begin(), src.end());
    }

    add_data(data);
    if (!compile()) {
        LOG_ERROR("shader failed to compile");
        return false;
    }

    return true;
}

void Shader::gui_inspector() {
    ImGui::Text("Name: %s", name.c_str());
    ImGui::Text("Program ID: %u", reference);
    ImGui::Text("Deferred: %s", deferred_ ? "Yes" : "No");

    ImGui::Columns(2, "component_table");
    ImGui::Separator();
    ImGui::Text("Type"); ImGui::NextColumn();
    ImGui::Text("ID"); ImGui::NextColumn();
    ImGui::Separator();

    for (auto const &c: components) {
        ImGui::Text("%s", shader_component_to_str(c.first)); ImGui::NextColumn();
        ImGui::Text("%d", c.second); ImGui::NextColumn();
    }
    ImGui::Columns(1);
    ImGui::Separator();

    if (ImGui::Button("Recompile")) {
        recompile();
    }
}

shader_property_visitor::shader_property_visitor(
        const GLuint &_program_id,
        const char *_uniform_name)
        : program_id(_program_id)
        , uniform_name(_uniform_name)
{}

void shader_property_visitor::operator()(const bool &_value) const
{
    glUniform1i(glGetUniformLocation(program_id, uniform_name), _value);
}

void shader_property_visitor::operator()(const int &_value) const
{
    glUniform1i(glGetUniformLocation(program_id, uniform_name), _value);
}

void shader_property_visitor::operator()(const float &_value) const
{
    glUniform1f(glGetUniformLocation(program_id, uniform_name), _value);
}

void shader_property_visitor::operator()(const GLuint &_value) const
{
    glUniform1ui(glGetUniformLocation(program_id, uniform_name), _value);
}

void shader_property_visitor::operator()(const glm::vec2 &_value) const
{
    glUniform2fv(glGetUniformLocation(program_id, uniform_name), 1, glm::value_ptr(_value));
}

void shader_property_visitor::operator()(const glm::vec3 &_value) const
{
    glUniform3fv(glGetUniformLocation(program_id, uniform_name), 1, glm::value_ptr(_value));
}

void shader_property_visitor::operator()(const glm::vec4 &_value) const
{
    glUniform4fv(glGetUniformLocation(program_id, uniform_name), 1, glm::value_ptr(_value));
}

void shader_property_visitor::operator()(const glm::mat2 &_value) const
{
    glUniformMatrix2fv(glGetUniformLocation(program_id, uniform_name), 1, GL_FALSE, glm::value_ptr(_value));
}

void shader_property_visitor::operator()(const glm::mat3 &_value) const
{
    glUniformMatrix3fv(glGetUniformLocation(program_id, uniform_name), 1, GL_FALSE, glm::value_ptr(_value));
}

void shader_property_visitor::operator()(const glm::mat4 &_value) const
{
    glUniformMatrix4fv(glGetUniformLocation(program_id, uniform_name), 1, GL_FALSE, glm::value_ptr(_value));
}

ShaderHandler::ShaderHandler()
: AssetHandler("shader") {}

void ShaderHandler::load(const resource_ptr& _resource) {
    auto shader = std::make_shared<Shader>("<UNKNOWN>");

    if (shader->load(_resource)) {
        shader->resource_ = _resource;
        items_[shader->get_name()] = shader;
    } else {
        throw std::runtime_error("shader compilation failed");
    }
}

void ShaderHandler::gui_inspector() {
    for (auto const &c: items_) {
        if (ImGui::TreeNode(c.first.c_str())) {
            c.second->gui_inspector();
            ImGui::TreePop();
        }
    }
}

const char* arc::shader_component_to_str(ShaderComponent _component) {
    switch (_component) {
        case ShaderComponentVertex:
            return "Vertex";
        case ShaderComponentGeometry:
            return "Geometry";
        case ShaderComponentFragment:
            return "Fragment";
        case ShaderComponentCompute:
            return "Compute";
        case ShaderComponentTessControl:
            return "Tessellation Control";
        case ShaderComponentTessEvaluation:
            return "Tessellation Evaluation";
        default:
            return "<Unknown>";
    }
}

void shader_property_gui_visitor::operator()(bool& _value) const {

}

void shader_property_gui_visitor::operator()(int& _value) const {
    ImGui::InputInt(nullptr, &_value);
}

void shader_property_gui_visitor::operator()(float& _value) const {
    ImGui::InputFloat(nullptr, &_value);
}

void shader_property_gui_visitor::operator()(GLuint& _value) const {
    // Not supported
}

void shader_property_gui_visitor::operator()(glm::vec2& _value) const {
    ImGui::InputFloat2(nullptr, glm::value_ptr(_value));
}

void shader_property_gui_visitor::operator()(glm::vec3& _value) const {
    ImGui::InputFloat3(nullptr, glm::value_ptr(_value));
}

void shader_property_gui_visitor::operator()(glm::vec4& _value) const {
    ImGui::InputFloat4(nullptr, glm::value_ptr(_value));
}

void shader_property_gui_visitor::operator()(glm::mat2& _value) const {
    // Not supported
}

void shader_property_gui_visitor::operator()(glm::mat3& _value) const {
    // Not supported
}

void shader_property_gui_visitor::operator()(glm::mat4& _value) const {
    // Not supported
}

const char* shader_property_name_visitor::operator()(bool) const {
    return "Bool";
}

const char* shader_property_name_visitor::operator()(int) const {
    return "Int";
}

const char* shader_property_name_visitor::operator()(float) const {
    return "Float";
}

const char* shader_property_name_visitor::operator()(GLuint) const {
    return "Uint";
}

const char* shader_property_name_visitor::operator()(glm::vec2) const {
    return "Vec2";
}

const char* shader_property_name_visitor::operator()(glm::vec3) const {
    return "Vec3";
}

const char* shader_property_name_visitor::operator()(glm::vec4) const {
    return "Vec4";
}

const char* shader_property_name_visitor::operator()(glm::mat2) const {
    return "Mat2";
}

const char* shader_property_name_visitor::operator()(glm::mat3) const {
    return "Mat3";
}

const char* shader_property_name_visitor::operator()(glm::mat4) const {
    return "Mat4";
}
