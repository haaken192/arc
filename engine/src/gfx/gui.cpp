/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <arc/gfx/gui.h>

#include <imgui/imgui.h>

#include <arc/gfx/texture.h>

using namespace arc;

void arc::gui_texture(const texture_ptr& _texture, float _height) {
    auto size = _texture->get_size();

    ImGui::Text("Texture type: GL_TEXTURE_2D");
    ImGui::Text("Reference: %u", _texture->get_reference());
    ImGui::Text("Size: %d, %d", size.x, size.y);

    auto scaled_h = int32_t((float(size.y) / float(size.x)) * _height);

    ImGui::Image(
            (ImTextureID)(intptr_t)_texture->get_reference(),
            ImVec2(_height, scaled_h),
            ImVec2(0,1), ImVec2(1,0),
            ImColor(255,255,255,255), ImColor(255,255,255,128));
}

