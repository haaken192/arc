/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <arc/gfx/framebuffer.h>

#include <arc/gfx/attachment.h>
#include <arc/gfx/texture.h>

using namespace arc;

GBuffer::GBuffer(glm::ivec2 _size, std::shared_ptr<AttachmentTexture2D> _depth, bool _hdr)
: Framebuffer(_size)
, hdr_(_hdr) {
    class_name = "GBuffer";
    name = "GBuffer";

    auto a0 = std::make_shared<AttachmentTexture2D>(TextureFormatRGBA32, _size);
    auto a1 = std::make_shared<AttachmentTexture2D>(TextureFormatRGBA32UI, _size);
    a1->get_object()->bind();
    a1->get_object()->set_filter(GL_NEAREST, GL_NEAREST);

    std::shared_ptr<AttachmentTexture2D> a2 = nullptr;

    if (hdr_) {
        a2 = std::make_shared<AttachmentTexture2D>(TextureFormatRGBA16, _size);
    } else {
        a2 = std::make_shared<AttachmentTexture2D>(TextureFormatDefaultColor, _size);
    }

    set_attachment(GL_COLOR_ATTACHMENT0, a0);
    set_attachment(GL_COLOR_ATTACHMENT1, a1);
    set_attachment(GL_COLOR_ATTACHMENT2, a2);
    set_attachment(GL_DEPTH_ATTACHMENT, _depth);

    set_draw_buffers({{GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2}});

    if (!validate()) {
        throw std::runtime_error("GBuffer failed to validate");
    }
}

bool GBuffer::is_hdr() const {
    return hdr_;
}

texture_ptr GBuffer::get_attachment_0() const {
    return get_attachment<AttachmentTexture2D>(GL_COLOR_ATTACHMENT0)->get_object();
}

const Texture2D* GBuffer::get_attachment_0_p() const {
    return get_attachment_p<AttachmentTexture2D>(GL_COLOR_ATTACHMENT0)->get_object().get();
}

texture_ptr GBuffer::get_attachment_1() const {
    return get_attachment<AttachmentTexture2D>(GL_COLOR_ATTACHMENT1)->get_object();
}

const Texture2D* GBuffer::get_attachment_1_p() const {
    return get_attachment_p<AttachmentTexture2D>(GL_COLOR_ATTACHMENT1)->get_object().get();
}

texture_ptr GBuffer::get_attachment_2() const {
    return get_attachment<AttachmentTexture2D>(GL_COLOR_ATTACHMENT2)->get_object();
}

const Texture2D* GBuffer::get_attachment_2_p() const {
    return get_attachment_p<AttachmentTexture2D>(GL_COLOR_ATTACHMENT2)->get_object().get();
}

texture_ptr GBuffer::get_attachment_depth() const {
    return get_attachment<AttachmentTexture2D>(GL_DEPTH_ATTACHMENT)->get_object();
}

const Texture2D* GBuffer::get_attachment_depth_p() const {
    return get_attachment_p<AttachmentTexture2D>(GL_DEPTH_ATTACHMENT)->get_object().get();
}
