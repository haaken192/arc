/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <arc/gfx/renderer.h>

#include <arc/core/scene.h>
#include <arc/gfx/framebuffer.h>
#include <arc/gfx/mesh.h>
#include <arc/gfx/shader.h>
#include <arc/gfx/skybox.h>
#include <arc/gfx/texture.h>
#include <arc/gfx/attachment.h>
#include <arc/gfx/gui.h>
#include <arc/gfx/ssao.h>
#include <arc/core/window.h>
#include <arc/core/input.h>

using namespace arc;

static std::shared_ptr<Texture2D> generate_brdf_lut() {
    auto fbo = std::make_shared<Framebuffer>();
    auto lut = std::make_shared<Texture2D>(TextureFormatRG16, glm::ivec2(512));
    auto shader = AssetManager::get_asset<Shader>("pbr/brdf");
    auto mesh = create_mesh_quad();

    lut->bind();
    lut->set_wrap(GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE, GL_CLAMP_TO_EDGE);
    lut->unbind();

    fbo->bind();
    glViewport(0, 0, 512, 512);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, lut->get_reference(), 0);

    shader->bind();
    mesh->bind();

    mesh->draw();

    mesh->unbind();
    shader->unbind();
    fbo->unbind();

    return lut;
}

Renderer::Renderer()
: active_mode_(RenderModeDeferred) {
    back_mesh_ = create_mesh_quad();
    fbo_ = std::make_shared<Framebuffer>(Window::get_resolution());

    tex_gbuffer_0_ = std::make_shared<AttachmentTexture2D>(TextureFormatRGBA32, fbo_->get_size());
    tex_gbuffer_1_ = std::make_shared<AttachmentTexture2D>(TextureFormatRGBA32UI, fbo_->get_size());

    tex_gbuffer_1_->get_object()->bind();
    tex_gbuffer_1_->get_object()->set_filter(GL_NEAREST, GL_NEAREST);
    tex_gbuffer_1_->get_object()->unbind();

    tex_hdr_target_ = std::make_shared<AttachmentTexture2D>(TextureFormatDefaultHDRColor, fbo_->get_size());
    tex_ldr_target_ = std::make_shared<AttachmentTexture2D>(TextureFormatDefaultColor, fbo_->get_size());

    tex_depth_ = std::make_shared<AttachmentTexture2D>(TextureFormatDepth24, fbo_->get_size());

    shader_ = AssetManager::get_asset_ptr<Shader>("standard");
    skybox_shader_ = AssetManager::get_asset_ptr<Shader>("utils/skybox");
    ssao_ = std::make_unique<SSAO>();
    tonemap_ = std::make_unique<Tonemap>();

    fbo_->set_attachment(GL_DEPTH_ATTACHMENT, tex_depth_);

    /* Validate GBuffer attachments */
    fbo_->set_attachment(GL_COLOR_ATTACHMENT0, tex_gbuffer_0_);
    fbo_->set_attachment(GL_COLOR_ATTACHMENT1, tex_gbuffer_1_);
    if (!fbo_->validate()) {
        throw std::runtime_error("Renderer GBuffer attachments failed to validate");
    }

    fbo_->remove_attachment(GL_COLOR_ATTACHMENT0);
    fbo_->remove_attachment(GL_COLOR_ATTACHMENT1);

    /* Validate standard attachments */
    fbo_->set_attachment(GL_COLOR_ATTACHMENT0, tex_hdr_target_);
    if (!fbo_->validate()) {
        throw std::runtime_error("Renderer HDR attachments failed to validate");
    }

    /* Validate standard attachments */
    fbo_->set_attachment(GL_COLOR_ATTACHMENT0, tex_ldr_target_);
    if (!fbo_->validate()) {
        throw std::runtime_error("Renderer LDR attachments failed to validate");
    }

    fbo_->remove_attachment(GL_COLOR_ATTACHMENT0);

    tex_brdf_lut_ = generate_brdf_lut();
}

void Renderer::render(
        const Renderable* _renderable,
        const std::vector<drawable_ptr>& _drawables,
        const std::vector<light_ptr>& _lights) {
    if (Input::window_resized()) {
        resize();
    }

    active_mode_ = RenderModeDeferred;

    fbo_->bind();
    fbo_->set_attachment(GL_COLOR_ATTACHMENT0, tex_gbuffer_0_);
    fbo_->set_attachment(GL_COLOR_ATTACHMENT1, tex_gbuffer_1_);
    fbo_->set_draw_buffers({GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1});
    fbo_->update_attachments();
    fbo_->clear_buffers();

    for (auto const &v : _drawables) {
        if (v->is_deferrable()) {
            v->draw(_renderable);
        }
    }

    fbo_->remove_attachment(GL_COLOR_ATTACHMENT0);
    fbo_->remove_attachment(GL_COLOR_ATTACHMENT1);

    if (ssao_->enabled_) {
        ssao_->compute(
                tex_gbuffer_0_->get_object(),
                tex_gbuffer_1_->get_object(),
                tex_depth_->get_object(),
                _renderable
        );
    }

    fbo_->set_attachment(GL_COLOR_ATTACHMENT0, tex_hdr_target_);
    fbo_->set_draw_buffers({GL_COLOR_ATTACHMENT0});
    fbo_->update_attachments();
    fbo_->clear_buffer_flags(GL_COLOR_BUFFER_BIT);

    skybox_shader_->bind();
    skybox_shader_->set_uniform("v_view_matrix", _renderable->get_view_matrix());
    skybox_shader_->set_uniform("v_projection_matrix", _renderable->get_projection_matrix());

    if (auto skybox = SceneManager::get_active()->get_skybox()) {
        skybox->get_radiance()->activate(0);
    }

    glDepthMask(GL_FALSE);
    glDepthFunc(GL_LEQUAL);
    back_mesh_->bind();
    back_mesh_->draw();

    // Ambient pass

    shader_->bind();
    shader_->set_subroutine(ShaderComponentFragment, "deferred_pass_ambient");
    shader_->set_uniform("v_model_matrix", glm::mat4(1));
    shader_->set_uniform("v_view_matrix", glm::mat4(1));
    shader_->set_uniform("v_projection_matrix", glm::mat4(1));
    shader_->set_uniform("f_camera", _renderable->get_position());
    shader_->set_uniform("f_dimensions", glm::vec2(fbo_->get_size()));

    tex_gbuffer_0_->get_object()->activate(0);
    tex_gbuffer_1_->get_object()->activate(1);
    tex_depth_->get_object()->activate(2);

    if (auto skybox = SceneManager::get_active()->get_skybox()) {
        skybox->get_radiance()->activate(3);
        skybox->get_irradiance()->activate(4);
        skybox->get_specular()->activate(5);
    }
    tex_brdf_lut_->activate(6);

    back_mesh_->draw();
    back_mesh_->unbind();

    // TONEMAP - LDR from here on out
    fbo_->set_attachment(GL_COLOR_ATTACHMENT0, tex_ldr_target_);
    fbo_->set_draw_buffers({GL_COLOR_ATTACHMENT0});
    fbo_->update_attachments();
    fbo_->clear_buffer_flags(GL_COLOR_BUFFER_BIT);

    tex_hdr_target_->get_object()->activate(0);

    tonemap_->compute();

    glDepthFunc(GL_LESS);
    glDepthMask(GL_TRUE);

    // Forward render

    active_mode_ = RenderModeForward;

    for (auto const &v : _drawables) {
        if (!v->is_deferrable()) {
            v->draw(_renderable);
        }
    }

    fbo_->unbind();

    Framebuffer::blit_framebuffers(fbo_, Framebuffer::current(), GL_COLOR_ATTACHMENT0);
}

void Renderer::resize() {
    fbo_->set_size(Window::get_resolution());

    tex_gbuffer_0_->set_size(fbo_->get_size());
    tex_gbuffer_1_->set_size(fbo_->get_size());
    tex_hdr_target_->set_size(fbo_->get_size());
    tex_ldr_target_->set_size(fbo_->get_size());
    tex_depth_->set_size(fbo_->get_size());
}

RenderMode Renderer::get_active_mode() const {
    return active_mode_;
}

void Renderer::gui_inspector() {
    if (ImGui::CollapsingHeader("Textures")) {
        if (ImGui::TreeNode("Depth Buffer")) {
            gui_texture(tex_depth_->get_object());
            ImGui::TreePop();
        }
        if (ImGui::TreeNode("GBuffer Target 0")) {
            gui_texture(tex_gbuffer_0_->get_object());
            ImGui::TreePop();
        }
        if (ImGui::TreeNode("GBuffer Target 1")) {
            gui_texture(tex_gbuffer_1_->get_object());
            ImGui::TreePop();
        }
        if (ImGui::TreeNode("HDR Target")) {
            gui_texture(tex_hdr_target_->get_object());
            ImGui::TreePop();
        }
        if (ImGui::TreeNode("LDR Target")) {
            gui_texture(tex_ldr_target_->get_object());
            ImGui::TreePop();
        }
        if (ImGui::TreeNode("BRDF LUT")) {
            gui_texture(tex_brdf_lut_);
            ImGui::TreePop();
        }
    }

    if (ImGui::CollapsingHeader("SSAO")) {
        ssao_->gui_inspector();
    }
    if (ImGui::CollapsingHeader("Tonemap")) {
        tonemap_->gui_inspector();
    }
}
