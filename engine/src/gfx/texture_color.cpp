/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <arc/gfx/texture.h>

#include <glm/gtc/type_ptr.hpp>

using namespace arc;

TextureColor::TextureColor()
: TextureColor(glm::vec4(1)) {}

TextureColor::TextureColor(glm::vec4 _color)
: Texture(GL_TEXTURE_2D, TextureFormatRGBA32)
, color_(_color) {
    class_name = "TextureColor";
    name = "TextureColor";
    size_ = glm::ivec2(1, 1);
    resizable_ = false;

    bind();
    set_wrap_r(GL_CLAMP_TO_EDGE);
    set_wrap_s(GL_CLAMP_TO_EDGE);
    set_wrap_t(GL_CLAMP_TO_EDGE);
    set_filter(GL_NEAREST, GL_NEAREST);
    glTexImage2D(type_, 0, internal_, size_.x, size_.y, 0, glformat_, storage_, glm::value_ptr(color_));
    unbind();
}

void TextureColor::upload_data(void* _data) {
    bind();
    glTexImage2D(type_, 0, internal_, size_.x, size_.y, 0, glformat_, storage_, glm::value_ptr(color_));
    unbind();
}

void TextureColor::set_color(glm::vec4 _color) {
    color_ = _color;
    upload();
}

glm::vec4 TextureColor::get_color() const {
    return color_;
}
