/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <arc/gfx/texture.h>

#include <glm/gtc/matrix_transform.hpp>
#include <imgui/imgui.h>

#include <arc/core/log.h>
#include <arc/core/resource.h>
#include <arc/gfx/framebuffer.h>
#include <arc/gfx/image.h>
#include <arc/gfx/mesh.h>
#include <arc/gfx/shader.h>

using namespace arc;

TextureCubemap::TextureCubemap(TextureFormat _format)
: TextureCubemap(_format, 0) {}

TextureCubemap::TextureCubemap(TextureFormat _format, int32_t _size)
: Texture(GL_TEXTURE_CUBE_MAP, _format)
{
    class_name = "TextureCubemap";
    name = "TextureCubemap";
    size_ = glm::ivec2(_size);
    resizable_ = false;

    bind();
    upload();
}

void TextureCubemap::upload_data(void* _data) {
    bind();
    for (uint32_t i = 0; i < 6; i++) {
        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X+i, 0, internal_, size_.x, size_.y, 0, glformat_, storage_, nullptr);
    }
    unbind();
}

void TextureCubemap::upload_data(void* _data, uint32_t _face) {
    bind();
    if (_face < 6) {
        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X+_face, 0, internal_, size_.x, size_.y, 0, glformat_, storage_, _data);
    }
    unbind();
}

bool TextureCubemap::load(const resource_ptr& _resource) {
    return false;
}

void TextureCubemap::gui_inspector() {
    ImGui::Text("Texture type: GL_TEXTURE_CUBE_MAP");
    ImGui::Text("Reference: %u", reference_);
    ImGui::Text("Size: %d, %d", size_.x, size_.y);
}

CubemapHandler::CubemapHandler()
: AssetHandler("cubemap") {}

void CubemapHandler::load(const resource_ptr& _resource) {
    if (auto c = load_cubemap(_resource)) {
        items_[c->get_name()] = c;
    }
}

void CubemapHandler::gui_inspector() {
    for (auto const &c: items_) {
        if (ImGui::TreeNode(c.first.c_str())) {
            c.second->gui_inspector();
            ImGui::TreePop();
        }
    }
}
