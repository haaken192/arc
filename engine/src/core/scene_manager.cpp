/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "arc/core/scene.h"

using namespace arc;

SceneManager* SceneManager::ref__ = nullptr;

SceneManager::SceneManager()
: System("scene_manager") {
    ref__ = this;
}

SceneManager::~SceneManager() {
    ref__ = nullptr;
}

void SceneManager::add(scene_ptr _scene) {
    ref__->scenes_[_scene->get_name()] = _scene;
}

void SceneManager::load(const std::string &_name) {
    auto scene = ref__->scenes_.at(_name);

    scene->load();
}

void SceneManager::purge_push(const std::string &_name) {
    remove_all();
    push(_name);
}

void SceneManager::replace(const std::string &_name) {
    pop();
    push(_name);
}

void SceneManager::push(const std::string &_name) {
    if (!ref__->active_.empty()) {
        auto prev = ref__->active_.top();
        ref__->scenes_[prev]->on_deactivate();
    }

    ref__->active_.push(_name);
    if (!ref__->scenes_[_name]->loaded_) {
        ref__->scenes_[_name]->load();
    }
    ref__->scenes_[_name]->on_activate();
}

void SceneManager::pop() {
    if (!ref__->active_.empty()) {
        auto current = ref__->active_.top();
        ref__->scenes_[current]->on_deactivate();
        ref__->active_.pop();
    }

    if (!ref__->active_.empty()) {
        auto current = ref__->active_.top();
        ref__->scenes_[current]->on_activate();
    }
}

void SceneManager::remove_all() {
    while (!ref__->active_.empty()) {
        ref__->active_.pop();
    }
}

Scene* SceneManager::get_active() {
    if (!ref__->active_.empty()) {
        auto const &top = ref__->active_.top();
        if (!top.empty()) {
            return ref__->scenes_.at(top).get();
        }
    }

    return nullptr;
}

std::string SceneManager::get_active_name() {
    if (!ref__->active_.empty()) {
        return ref__->active_.top();
    }

    return "";
}

void SceneManager::update() {
    if (auto s = get_active()) {
        s->update();
    }
}

void SceneManager::fixed_update() {
    if (auto s = get_active()) {
        s->fixed_update();
    }
}

void SceneManager::display() {
    if (auto s = get_active()) {
        s->display();
    }
}