/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <algorithm>

#include "arc/core/object.h"

using namespace arc;

ObjectManager* ObjectManager::ref = nullptr;

ObjectManager::ObjectManager()
: System("object_manager") {
	ref = this;
	next = 1;
}

ObjectManager::~ObjectManager() {
	ref = nullptr;
}

arc::id_t ObjectManager::assign() {
	if (ref->objects.empty()) {
		ref->objects.insert(1);
		ref->next = 2;
		return 1;
	}

	auto next_itr = std::find(ref->objects.cbegin(), ref->objects.cend(), ref->next);
	if (next_itr != ref->objects.cend())
	{
		while (true)
		{
			if (next_itr == ref->objects.cend())
				break;
			if (*next_itr != ref->next)
				break;
			ref->next++;
			++next_itr;
		}
	}

	id_t now = ref->next;
	ref->objects.insert(now);
	ref->next++;

	return now;
}

void ObjectManager::release(arc::id_t id) {
	ref->objects.erase(id);
}

void ObjectManager::release_all() {
	ref->objects.clear();
}

size_t ObjectManager::count() {
	return ref->objects.size();
}
