/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <GLFW/glfw3.h>

#include "arc/core/time.h"

using namespace arc;

const double FIXED_TIME = 0.05;

Time* Time::ref = nullptr;

Time::Time()
: System("time")
, frame_time(0)
, delta_time(0)
, next_logic_tick(0)
, frame(0) {
	ref = this;
}

Time::~Time() {
	ref = nullptr;
}

double Time::get_frame_time() {
	return ref->frame_time;
}

double Time::get_fixed_time() {
	return FIXED_TIME;
}

double Time::get_delta() {
	return ref->delta_time;
}

uint64_t Time::get_frame() {
	return ref->frame;
}

double Time::now() {
	return glfwGetTime();
}

void Time::frame_start() {
	frame_time = now();
}

void Time::frame_end() {
	delta_time = now() - frame_time;
	frame++;
}

void Time::logic_tick() {
	next_logic_tick += FIXED_TIME;
}

bool Time::logic_update() const {
	return now() > next_logic_tick;
}
