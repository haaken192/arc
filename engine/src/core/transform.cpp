/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <glm/gtc/matrix_transform.hpp>

#include <imgui/imgui.h>
#include <glm/gtc/type_ptr.hpp>

#include "arc/core/transform.h"
#include "arc/core/entity.h"
#include "arc/core/log.h"

using namespace arc;

Transform::Transform()
: model_matrix_(glm::mat4(1.0f))
, active_matrix_(glm::mat4(1.0f))
, rotation_(glm::fquat(1.0f, 0.0f, 0.0f, 0.0f))
, position_(glm::vec3(0.0f))
, scale_(glm::vec3(1.0f)) {
    class_name = "Transform";
    recompute(false);
}

glm::mat4 Transform::get_model_matrix() const {
    return model_matrix_;
}

glm::mat4 Transform::get_active_matrix() const {
    return active_matrix_;
}

glm::fquat Transform::get_rotation() const {
    return rotation_;
}

glm::vec3 Transform::get_position() const {
    return position_;
}

glm::vec3 Transform::get_scale() const {
    return scale_;
}

void Transform::set_rotation(const glm::fquat &_rotation, bool _propagate) {
    rotation_ = _rotation;
    if (_propagate) {
        recompute(true);
    }
}

void Transform::set_position(const glm::vec3 &_position, bool _propagate) {
    position_ = _position;
    if (_propagate) {
        recompute(true);
    }
}

void Transform::set_scale(const glm::vec3 &_scale, bool _propagate) {
    scale_ = _scale;
    if (_propagate) {
        recompute(true);
    }
}

void Transform::recompute(bool _propagate) {
    model_matrix_ = glm::translate(glm::mat4(1), position_) * glm::mat4_cast(rotation_) * glm::scale(glm::mat4(1), scale_);
    active_matrix_ = model_matrix_;

    if (entity) {
        entity->send_message(MessageTransform);
        if (auto parent = entity->get_parent()) {
            active_matrix_ = parent->get_transform()->get_active_matrix() * model_matrix_;
        }

        if (_propagate) {
            auto children = entity->get_components_in_children<Transform>();
            for (const auto &c : children) {
                c->recompute(false);
            }
        }
    }
}

void Transform::gui_inspector() {
    glm::vec3 rot = glm::eulerAngles(rotation_);
    if (ImGui::DragFloat3("Position", glm::value_ptr(position_), 0.01f, -10000.0f, 10000.0f)) {
        set_position(position_, true);
    }
    if (ImGui::DragFloat3("Rotation (New)", glm::value_ptr(rot), 0.01f, -10000.0f, 10000.0f)) {
        set_rotation(glm::fquat(rot), true);
    }
    if (ImGui::DragFloat3("Scale", glm::value_ptr(scale_), 0.01f, 0.0f, 1000.0f)) {
        set_scale(scale_, true);
    }
}
