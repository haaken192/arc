/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <csignal>
#include <iostream>

#include <arc/core/app.h>
#include <arc/core/asset.h>
#include <arc/core/log.h>
#include <arc/core/object.h>
#include <arc/core/scene.h>
#include <arc/core/time.h>
#include <arc/core/window.h>
#include <arc/core/input.h>

using namespace arc;

std::string App::name;
std::string App::company_name;
std::string App::version;
bool App::running = false;

const uint8_t MAX_FRAME_SKIP = 5;

void signal_exit(int signal) {
    App::quit();
}

App::App(const Properties &_properties)
: log_(std::make_unique<Log>()) {
    name = _properties.name;
    company_name = _properties.company_name;
    version = _properties.version;
    running = false;

    if (_properties.pre_setup_func) {
        _properties.pre_setup_func();
    }

    systems.push_front(std::make_unique<ObjectManager>());
    systems.push_front(std::make_unique<Window>());
    systems.push_front(std::make_unique<Input>());
    systems.push_front(std::make_unique<AssetManager>());
    systems.push_front(std::make_unique<Time>());
    systems.push_front(std::make_unique<SceneManager>());

    if (_properties.post_setup_func) {
        _properties.post_setup_func();
    }
}

void App::run() {
    running = true;

    std::signal(SIGINT, signal_exit);
    std::signal(SIGABRT, signal_exit);
    std::signal(SIGTERM, signal_exit);

    auto window = Window::ref;
    auto time = Time::ref;
    auto scene = SceneManager::ref__;

    uint8_t loops = 0;

    while (running) {
        time->frame_start();
        window->handle_events();

        scene->update();

        loops = 0;
        while (time->logic_update() && loops < MAX_FRAME_SKIP) {
            time->logic_tick();
            scene->fixed_update();
            loops++;
        }

        window->begin_frame();
        scene->display();
        window->draw_gui();
        window->end_frame();

        time->frame_end();
    }

    teardown();
}

void App::teardown() {
    LOG_INFO("app teardown");
}

void App::quit() {
    running = false;
}

std::string App::get_name() {
    return name;
}

std::string App::get_company_name() {
    return company_name;
}

std::string App::get_version() {
    return version;
}
