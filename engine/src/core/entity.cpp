/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <arc/core/entity.h>

#include <arc/core/asset.h>
#include <arc/scene/camera.h>
#include <arc/scene/light.h>
#include <arc/gfx/material.h>
#include <arc/scene/mesh_renderer.h>
#include <arc/core/scene.h>
#include <arc/core/transform.h>
#include <arc/gfx/mesh.h>
#include <arc/gfx/shader.h>
#include <arc/gfx/texture.h>

using namespace arc;

Entity::Entity(const std::string &_name)
: active_self(true)
, active_hierarchy(true) {
    name = _name;
    class_name = "Entity";

    components.push_back(std::make_shared<Transform>());
}

bool Entity::is_active() const {
    return active_self && active_hierarchy;
}

bool Entity::is_active_self() const {
    return active_self;
}

bool Entity::is_active_in_hierarchy() const {
    return active_hierarchy;
}

void Entity::set_active(bool _active) {
    if (active_self != _active) {
        active_self = _active;

        if (scene) {
            scene->set_entity_active(id, _active);
        }
    }
}

void Entity::set_transform(transform_ptr _transform) {
    components[0] = _transform;
}

void Entity::add_component(component_ptr _component) {
    for (auto &c : components) {
        if (c->get_id() == _component->get_id()) {
            return;
        }
    }

    _component->entity = shared_from_this();
    components.push_back(_component);
}

entity_p Entity::get_parent() const {
    if (scene) {
        return scene->entity_parent(id).get();
    }

    return nullptr;
}

scene_p Entity::get_scene() const {
    return scene.get();
}

transform_ptr Entity::get_transform() const {
    return std::dynamic_pointer_cast<Transform>(components[0]);
}

std::vector<component_ptr> Entity::_get_components_in_children() const {
    if (scene) {
        return scene->entity_components_in_children(id);
    }

    return std::vector<component_ptr>{};
}

std::vector<component_ptr> Entity::_get_components_in_parent() const {
    if (scene) {
        return scene->entity_components_in_parent(id);
    }

    return std::vector<component_ptr>{};
}

void Entity::gui_inspector() {

}

void Entity::send_message(Message _m) {
    if (!active_hierarchy) {
        return;
    }

    for (auto const &c : components) {
        switch (_m) {
            case MessageStart:
                if (!c->entity) {
                    c->entity = shared_from_this();
                }
                if (auto v = dynamic_cast<ScriptComponent*>(c.get())) {
                    v->start();
                }
                break;
            case MessageActivate:
            case MessageAwake:
                if (auto v = dynamic_cast<ScriptComponent*>(c.get())) {
                    v->awake();
                }
                break;
            case MessageUpdate:
                if (auto v = dynamic_cast<ScriptComponent*>(c.get())) {
                    v->update();
                }
                break;
            case MessageLateUpdate:
                if (auto v = dynamic_cast<ScriptComponent*>(c.get())) {
                    v->late_update();
                }
                break;
            case MessageFixedUpdate:
                if (auto v = dynamic_cast<ScriptComponent*>(c.get())) {
                    v->fixed_update();
                }
                break;
            case MessageTransform:
                c->on_transform_changed();
                break;
            default:
                break;
        }
    }
}

entity_ptr arc::create_entity(const std::string& _name) {
    return std::make_shared<Entity>(_name);
}

entity_ptr arc::create_camera(const std::string& _name) {
    auto e = std::make_shared<Entity>(_name);
    e->add_component(std::make_shared<Camera>());

    return e;
}

entity_ptr arc::create_quad(const std::string& _name) {
    auto e = create_entity(_name);

    auto m = std::make_shared<Material>(AssetManager::get_asset_ptr<Shader>("screen2"));
    auto mr = std::make_shared<MeshRenderer>(create_mesh_quad(), m);

    e->add_component(mr);

    return e;
}

entity_ptr arc::create_3d_object(const std::string& _name, const std::string& _object) {
    auto e = create_entity(_name);
    auto m = std::make_shared<Material>(AssetManager::get_asset_ptr<Shader>("standard"));
    auto mr = std::make_shared<MeshRenderer>(AssetManager::get_asset_ptr<Mesh>(_object), m);
    e->add_component(mr);

    return e;
}

entity_ptr arc::create_light(const std::string& _name, LightType _type) {
    auto e = create_entity(_name);
    auto l = std::make_shared<Light>(_type);
    e->add_component(l);

    return e;
}
