/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <arc/core/input.h>

using namespace arc;

Input* Input::__ref = nullptr;

Input::Input()
: System("input")
, mouse_button_events_(std::vector<EventMouseButton>{})
, key_events_(std::vector<EventKey>{})
, joy_events_(std::vector<EventJoy>{})
, scroll_axis_(glm::dvec2(0))
, cursor_position_(glm::ivec2(0))
, scroll_moved_(false)
, cursor_moved_(false)
, window_resized_(false)
, has_events_(false) {
    __ref = this;
}

Input::~Input() {
    __ref = nullptr;
}

void Input::reset() {
    mouse_button_events_.clear();
    key_events_.clear();
    joy_events_.clear();

    scroll_axis_ = glm::dvec2(0);
    cursor_position_ = glm::ivec2(0);
    scroll_moved_ = false;
    cursor_moved_ = false;
    window_resized_= false;
    has_events_ = false;
}

void Input::defer_gui_events() {

}

bool Input::key_down(int _key) {
    for (auto const &c : __ref->key_events_) {
        if (c.key_ == _key && (c.action_ == GLFW_PRESS || c.action_ == GLFW_REPEAT)) {
            return true;
        }
    }

    return false;
}

bool Input::key_up(int _key) {
    for (auto const &c : __ref->key_events_) {
        if (c.key_ == _key && c.action_ == GLFW_RELEASE) {
            return true;
        }
    }

    return false;
}

bool Input::key_pressed(int _key) {
    return !__ref->key_events_.empty();
}

bool Input::mouse_down(int _button) {
    for (auto const &c : __ref->mouse_button_events_) {
        if (c.button_ == _button && c.action_ == GLFW_PRESS) {
            return true;
        }
    }

    return false;
}

bool Input::mouse_up(int _button) {
    for (auto const &c : __ref->mouse_button_events_) {
        if (c.button_ == _button && c.action_ == GLFW_RELEASE) {
            return true;
        }
    }

    return false;
}

bool Input::mouse_wheel() {
    return __ref->scroll_moved_;
}

bool Input::mouse_moved() {
    return __ref->cursor_moved_;
}

bool Input::mouse_pressed() {
    return !__ref->mouse_button_events_.empty();
}

bool Input::window_resized() {
    return __ref->window_resized_;
}

bool Input::has_events() {
    return __ref->has_events_;
}

double Input::mouse_wheel_x() {
    return __ref->scroll_axis_.x;
}

double Input::mouse_wheel_y() {
    return __ref->scroll_axis_.y;
}

glm::ivec2 Input::mouse_position() {
    return __ref->cursor_position_;
}
