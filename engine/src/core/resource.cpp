/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <arc/core/resource.h>

#include <fstream>
#include <iostream>

using namespace arc;

static inline bool has_prefix(const std::string &_value, const std::string &_prefix) {
    return _value.find(_prefix) == 0;
}

static inline std::string trim_prefix(const std::string &_value, const std::string &_prefix) {
    auto found = _value.find(_prefix);
    if (found != std::string::npos) {
        return _value.substr(found+1);
    }

    return _value;
}

static inline std::string path_base(const std::string &_path) {
    auto found = _path.find_last_of('/');
    if (found != std::string::npos) {
        return _path.substr(found+1);
    }

    return _path;
}

static inline std::string path_dir(const std::string &_path) {
    auto found = _path.find_last_of('/');
    if (found != std::string::npos) {
        return _path.substr(0, found);
    }

    return _path;
}

static inline std::string path_ext(const std::string &_path) {
    auto found = _path.find_last_of('.');
    if (found != std::string::npos) {
        return _path.substr(found+1);
    }

    return "";
}

static inline std::string replace_all(std::string str, const std::string &from, const std::string &to) {
    size_t start = 0;
    while ((start = str.find(from, start)) != std::string::npos) {
        str.replace(start, from.length(), to);
        start += to.length();
    }

    return str;
}

static inline std::string path_clean(std::string _str, Resource::Type _type) {
    if (_type == Resource::TypeFile) {
        return std::move(_str);
    }

    if (!has_prefix(_str, "..")) {
        _str = trim_prefix(_str, ".");
    }

    _str = replace_all(_str, "\\", "/");
    _str = trim_prefix(_str, "/");

    return _str;
}


Resource::Resource(const std::string& _filename)
: type_(TypeFile)
, location_(_filename) {
    // TODO: Add package support
    std::ifstream file(_filename, std::ios::binary);
    if (!file.is_open()) {
        throw std::runtime_error("Failed to open file: " + _filename);
    }

    file.unsetf(std::ios::skipws);
    std::streampos file_size;

    file.seekg(0, std::ios::end);
    file_size = file.tellg();
    file.seekg(0, std::ios::beg);

    data_ = std::vector<char>(static_cast<size_t>(file_size));
    file.read(data_.data(), file_size);
}

std::vector<char> Resource::bytes() const {
    return data_;
}

const char* Resource::data() const {
    return data_.data();
}

std::string Resource::string() const {
    return std::string(data_.data());
}

std::istringstream Resource::stream() const {
    return std::istringstream(data_.data());
}

size_t Resource::size() const {
    return data_.size();
}

std::string Resource::location() const {
    return location_;
}

std::string Resource::container() const {
    return container_;
}

std::string Resource::base() const {
    return path_base(location_);
}

std::string Resource::dir() const {
    return path_clean(path_dir(location_), type_);
}

std::string Resource::dir_prefix() const {
    if (type_ == TypeFile) {
        return dir();
    }

    return container_ + ":" + dir();
}

std::string Resource::ext() const {
    return path_ext(location_);
}

Resource::Type Resource::type() const {
    return type_;
}

void Resource::reload() {
    if (type_ == TypeFile) {
        std::ifstream file(location_, std::ios::binary);
        if (!file.is_open()) {
            throw std::runtime_error("Failed to open file: " + location_);
        }

        file.unsetf(std::ios::skipws);
        std::streampos file_size;

        file.seekg(0, std::ios::end);
        file_size = file.tellg();
        file.seekg(0, std::ios::beg);

        data_.reserve(static_cast<size_t>(file_size));
        file.read(data_.data(), file_size);
    }
}
