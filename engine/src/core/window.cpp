/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <arc/core/window.h>

#include <glm/gtc/matrix_transform.hpp>

#include <arc/core/app.h>
#include <arc/core/log.h>
#include <arc/gfx/builtin.h>
#include <arc/gfx/shader.h>
#include <arc/core/time.h>
#include <arc/core/input.h>

using namespace arc;

Window* Window::ref = nullptr;

static const char* arc_get_clipboard_text(void* user_data)
{
    return glfwGetClipboardString((GLFWwindow*)user_data);
}

static void arc_set_clipboard_text(void* user_data, const char* text)
{
    glfwSetClipboardString((GLFWwindow*)user_data, text);
}

Window::Window()
: System("window")
, ortho_matrix(glm::mat4(1.0f))
, resolution(glm::vec2(1280, 720))
, aspect_ratio(1)
, mouse_relative(false)
, gui_vao(0)
, gui_vbo(0)
, gui_ibo(0)
, gui_font_texture(0)
, show_demo_gui(true) {
    ref = this;

    if (!glfwInit()) {
        LOG_ERROR("Failed to initialize GLFW");
        exit(1);
    }

    LOG_DEBUG("GLFW initialized");

    glfwWindowHint(GLFW_RESIZABLE, true);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);

    window = glfwCreateWindow(resolution.x, resolution.y, App::get_name().c_str(), nullptr, nullptr);
    if (!window) {
        LOG_ERROR("Window failed to alloc");
        exit(1);
    }

    glfwSetCharCallback(window, Window::cb_char);
    glfwSetScrollCallback(window, Window::cb_scroll);
    glfwSetKeyCallback(window, Window::cb_key);
    glfwSetMouseButtonCallback(window, Window::cb_mouse_button);
    glfwSetWindowSizeCallback(window, Window::cb_window_resize);
    glfwSetCursorPosCallback(window, Window::cb_mouse_move);
    glfwSetJoystickCallback(Window::cb_joystick_event);

    glfwMakeContextCurrent(window);

    if (gl3wInit()) {
        LOG_ERROR("Failed to initialize OpenGL");
        exit(1);
    }

    if (!gl3wIsSupported(4, 3)) {
        LOG_ERROR("OpenGL 4.3 is not supported on this system");
        exit(1);
    }

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
    glClearColor(0.0, 0.0, 0.0, 1.0);

    glfwSwapInterval(1);

    init_gui();
    set_size(resolution);

    LOG_DEBUG("Initialized OpenGL: {} GLSL: {} ", glGetString(GL_VERSION), glGetString(GL_SHADING_LANGUAGE_VERSION));
}

Window::~Window() {
    glDeleteTextures(1, &gui_font_texture);
    glDeleteBuffers(1, &gui_ibo);
    glDeleteBuffers(1, &gui_vbo);
    glDeleteVertexArrays(1, &gui_vao);

    glfwDestroyWindow(window);
    glfwTerminate();

    ref = nullptr;
}

void Window::handle_events() {
    Input::__ref->reset();

    glfwPollEvents();

    if (glfwWindowShouldClose(ref->window)) {
        App::quit();
    }

    ImGuiIO& io = ImGui::GetIO();
    if (io.WantCaptureMouse || io.WantCaptureKeyboard) {
        Input::__ref->mouse_button_events_.clear();
        Input::__ref->scroll_axis_ = glm::dvec2(0);
        Input::__ref->cursor_position_ = glm::ivec2(0);
        Input::__ref->scroll_moved_ = false;
        Input::__ref->cursor_moved_ = false;
    }
    if (io.WantCaptureKeyboard) {
        Input::__ref->key_events_.clear();
        Input::__ref->joy_events_.clear();
    }
}

void Window::begin_frame() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    ImGuiIO& io = ImGui::GetIO();
    IM_ASSERT(io.Fonts->IsBuilt());

    int w = 0;
    int h = 0;
    int display_w = 0;
    int display_h = 0;

    glfwGetWindowSize(window, &w, &h);
    glfwGetFramebufferSize(window, &display_w, &display_h);

    io.DisplaySize = ImVec2(static_cast<float>(w), static_cast<float>(h));
    io.DisplayFramebufferScale = ImVec2(w > 0 ? (static_cast<float>(display_w) / w) : 0, h > 0 ? (static_cast<float>(display_h) / h) : 0);
    io.DeltaTime = Time::get_delta()  > 0.0 ? static_cast<float>(Time::get_delta()) : 1.0f/60.0f;

    update_mouse_pos_and_buttons();
    update_mouse_cursor();

    ImGui::NewFrame();
}

void Window::end_frame() {
    glfwSwapBuffers(ref->window);
}

void Window::init_gui() {
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();

    ImGuiIO& io = ImGui::GetIO();
    io.BackendFlags |= ImGuiBackendFlags_HasMouseCursors;
    io.BackendFlags |= ImGuiBackendFlags_HasSetMousePos;
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;

    io.KeyMap[ImGuiKey_Tab] = GLFW_KEY_TAB;
    io.KeyMap[ImGuiKey_LeftArrow] = GLFW_KEY_LEFT;
    io.KeyMap[ImGuiKey_RightArrow] = GLFW_KEY_RIGHT;
    io.KeyMap[ImGuiKey_UpArrow] = GLFW_KEY_UP;
    io.KeyMap[ImGuiKey_DownArrow] = GLFW_KEY_DOWN;
    io.KeyMap[ImGuiKey_PageUp] = GLFW_KEY_PAGE_UP;
    io.KeyMap[ImGuiKey_PageDown] = GLFW_KEY_PAGE_DOWN;
    io.KeyMap[ImGuiKey_Home] = GLFW_KEY_HOME;
    io.KeyMap[ImGuiKey_End] = GLFW_KEY_END;
    io.KeyMap[ImGuiKey_Insert] = GLFW_KEY_INSERT;
    io.KeyMap[ImGuiKey_Delete] = GLFW_KEY_DELETE;
    io.KeyMap[ImGuiKey_Backspace] = GLFW_KEY_BACKSPACE;
    io.KeyMap[ImGuiKey_Space] = GLFW_KEY_SPACE;
    io.KeyMap[ImGuiKey_Enter] = GLFW_KEY_ENTER;
    io.KeyMap[ImGuiKey_Escape] = GLFW_KEY_ESCAPE;
    io.KeyMap[ImGuiKey_A] = GLFW_KEY_A;
    io.KeyMap[ImGuiKey_C] = GLFW_KEY_C;
    io.KeyMap[ImGuiKey_V] = GLFW_KEY_V;
    io.KeyMap[ImGuiKey_X] = GLFW_KEY_X;
    io.KeyMap[ImGuiKey_Y] = GLFW_KEY_Y;
    io.KeyMap[ImGuiKey_Z] = GLFW_KEY_Z;

    io.SetClipboardTextFn = arc_set_clipboard_text;
    io.GetClipboardTextFn = arc_get_clipboard_text;
    io.ClipboardUserData = window;

    mouse_cursors[ImGuiMouseCursor_Arrow] = glfwCreateStandardCursor(GLFW_ARROW_CURSOR);
    mouse_cursors[ImGuiMouseCursor_TextInput] = glfwCreateStandardCursor(GLFW_IBEAM_CURSOR);
    mouse_cursors[ImGuiMouseCursor_ResizeAll] = glfwCreateStandardCursor(GLFW_ARROW_CURSOR);
    mouse_cursors[ImGuiMouseCursor_ResizeNS] = glfwCreateStandardCursor(GLFW_VRESIZE_CURSOR);
    mouse_cursors[ImGuiMouseCursor_ResizeEW] = glfwCreateStandardCursor(GLFW_HRESIZE_CURSOR);
    mouse_cursors[ImGuiMouseCursor_ResizeNESW] = glfwCreateStandardCursor(GLFW_ARROW_CURSOR);
    mouse_cursors[ImGuiMouseCursor_ResizeNWSE] = glfwCreateStandardCursor(GLFW_ARROW_CURSOR);
    mouse_cursors[ImGuiMouseCursor_Hand] = glfwCreateStandardCursor(GLFW_HAND_CURSOR);

    // GUI shader
    gui_shader = std::make_unique<Shader>("_INTERNAL_GUI_");
    gui_shader->add_data(SHADER_GUI);
    if (!gui_shader->compile()) {
        LOG_ERROR("GUI shader failed to compile");
        exit(1);
    }

    // GUI buffers
    glGenVertexArrays(1, &gui_vao);
    glGenBuffers(1, &gui_vbo);
    glGenBuffers(1, &gui_ibo);

    glBindVertexArray(gui_vao);
    glBindBuffer(GL_ARRAY_BUFFER, gui_vbo);
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glEnableVertexAttribArray(2);

    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(ImDrawVert), (GLvoid*)IM_OFFSETOF(ImDrawVert, pos));
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(ImDrawVert), (GLvoid*)IM_OFFSETOF(ImDrawVert, uv));
    glVertexAttribPointer(2, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(ImDrawVert), (GLvoid*)IM_OFFSETOF(ImDrawVert, col));

    glBindVertexArray(0);

    // GUI font texture
    unsigned char* pixels = nullptr;
    int width = 0;
    int height = 0;

    io.Fonts->GetTexDataAsRGBA32(&pixels, &width, &height);

    glGenTextures(1, &gui_font_texture);
    glBindTexture(GL_TEXTURE_2D, gui_font_texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);

    io.Fonts->TexID = (ImTextureID)(intptr_t)gui_font_texture;

    // Style

    ImGui::GetStyle().WindowRounding = 0;
}

void Window::draw_gui() {
//    ImGui::ShowDemoWindow(&show_demo_gui);

    ImGui::Render();
    ImDrawData* draw_data = ImGui::GetDrawData();

    ImGuiIO& io = ImGui::GetIO();
    int fb_width = (int)(draw_data->DisplaySize.x * io.DisplayFramebufferScale.x);
    int fb_height = (int)(draw_data->DisplaySize.y * io.DisplayFramebufferScale.y);
    if (fb_width <= 0 || fb_height <= 0) {
        return;
    }
    draw_data->ScaleClipRects(io.DisplayFramebufferScale);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, 0);

    glEnable(GL_BLEND);
    glBlendEquation(GL_FUNC_ADD);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDisable(GL_CULL_FACE);
    glDisable(GL_DEPTH_TEST);
    glEnable(GL_SCISSOR_TEST);

    glViewport(0, 0, (GLsizei)fb_width, (GLsizei)fb_height);

    gui_shader->bind();
    gui_shader->set_uniform("v_projection_matrix", ortho_matrix);
    glBindVertexArray(gui_vao);

    ImVec2 pos = draw_data->DisplayPos;
    for (int n = 0; n < draw_data->CmdListsCount; n++) {
        const ImDrawList* cmd_list = draw_data->CmdLists[n];
        const ImDrawIdx* idx_buffer_offset = nullptr;

        glBindBuffer(GL_ARRAY_BUFFER, gui_vbo);
        glBufferData(GL_ARRAY_BUFFER, (GLsizeiptr)cmd_list->VtxBuffer.Size * sizeof(ImDrawVert), (const GLvoid*)cmd_list->VtxBuffer.Data, GL_STREAM_DRAW);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gui_ibo);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, (GLsizeiptr)cmd_list->IdxBuffer.Size * sizeof(ImDrawIdx), (const GLvoid*)cmd_list->IdxBuffer.Data, GL_STREAM_DRAW);

        for (int cmd_i = 0; cmd_i < cmd_list->CmdBuffer.Size; cmd_i++)
        {
            const ImDrawCmd* pcmd = &cmd_list->CmdBuffer[cmd_i];
            if (pcmd->UserCallback)
            {
                // User callback (registered via ImDrawList::AddCallback)
                pcmd->UserCallback(cmd_list, pcmd);
            }
            else
            {
                ImVec4 clip_rect = ImVec4(pcmd->ClipRect.x - pos.x, pcmd->ClipRect.y - pos.y, pcmd->ClipRect.z - pos.x, pcmd->ClipRect.w - pos.y);
                if (clip_rect.x < fb_width && clip_rect.y < fb_height && clip_rect.z >= 0.0f && clip_rect.w >= 0.0f)
                {
                    // Apply scissor/clipping rectangle
                    glScissor((int)clip_rect.x, (int)(fb_height - clip_rect.w), (int)(clip_rect.z - clip_rect.x), (int)(clip_rect.w - clip_rect.y));

                    // Bind texture, Draw
                    glBindTexture(GL_TEXTURE_2D, (GLuint)(intptr_t)pcmd->TextureId);
                    glDrawElements(GL_TRIANGLES, (GLsizei)pcmd->ElemCount, sizeof(ImDrawIdx) == 2 ? GL_UNSIGNED_SHORT : GL_UNSIGNED_INT, idx_buffer_offset);
                }
            }
            idx_buffer_offset += pcmd->ElemCount;
        }
    }

    glBindVertexArray(0);
    gui_shader->unbind();

    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);
    glDisable(GL_SCISSOR_TEST);
}

void Window::set_size(const glm::ivec2 &_size) {
    resolution = _size;
    aspect_ratio = static_cast<float>(resolution.x) / static_cast<float>(resolution.y);
    ortho_matrix = glm::ortho(0.0f, static_cast<float>(resolution.x), static_cast<float>(resolution.y), 0.0f);

    ImGuiIO& io = ImGui::GetIO();
    io.DisplaySize.x = _size.x;
    io.DisplaySize.y = _size.y;

    glViewport(0, 0, _size.x, _size.y);
}

void Window::cb_char(GLFWwindow*, unsigned int _c) {
    ImGuiIO& io = ImGui::GetIO();
    if (_c > 0 && _c < 0x10000) {
        io.AddInputCharacter((unsigned short)_c);
    }
}

void Window::cb_key(GLFWwindow*, int _key, int _scancode, int _action, int _mods) {
    ImGuiIO& io = ImGui::GetIO();
    if (_action == GLFW_PRESS) {
        io.KeysDown[_key] = true;
    }
    if (_action == GLFW_RELEASE) {
        io.KeysDown[_key] = false;
    }

    (void)_mods;
    io.KeyCtrl = io.KeysDown[GLFW_KEY_LEFT_CONTROL] || io.KeysDown[GLFW_KEY_RIGHT_CONTROL];
    io.KeyShift = io.KeysDown[GLFW_KEY_LEFT_SHIFT] || io.KeysDown[GLFW_KEY_RIGHT_SHIFT];
    io.KeyAlt = io.KeysDown[GLFW_KEY_LEFT_ALT] || io.KeysDown[GLFW_KEY_RIGHT_ALT];
    io.KeySuper = io.KeysDown[GLFW_KEY_LEFT_SUPER] || io.KeysDown[GLFW_KEY_RIGHT_SUPER];

    Input::__ref->has_events_ = true;
    Input::__ref->key_events_.push_back(Input::EventKey{_scancode, _key, _action, _mods});
}

void Window::cb_scroll(GLFWwindow*, double _x, double _y) {
    ImGuiIO& io = ImGui::GetIO();
    io.MouseWheelH += (float)_x;
    io.MouseWheel += (float)_y;

    Input::__ref->has_events_ = true;
    Input::__ref->scroll_moved_ = true;
    Input::__ref->scroll_axis_.x += _x;
    Input::__ref->scroll_axis_.y += _y;
}

void Window::cb_mouse_button(GLFWwindow*, int _button, int _action, int _mods) {
    if (_action == GLFW_PRESS && _button >= 0 && _button < ref->mouse_just_pressed.size()) {
        ref->mouse_just_pressed[_button] = true;
    }

    Input::__ref->has_events_ = true;
    Input::__ref->mouse_button_events_.push_back(Input::EventMouseButton{_button, _action, _mods});
}

void Window::cb_window_resize(GLFWwindow*, int width, int height) {
    if (width > 0 && height > 0) {
        ref->set_size(glm::ivec2(width, height));

        Input::__ref->has_events_ = true;
        Input::__ref->window_resized_ = true;
    }
}

void Window::cb_mouse_move(GLFWwindow*, double _x, double _y) {
    Input::__ref->has_events_ = true;
    Input::__ref->cursor_position_ = glm::ivec2(_x, _y);
    Input::__ref->cursor_moved_ = true;
}

void Window::cb_joystick_event(int _joy, int _event) {
    Input::__ref->has_events_ = true;
    Input::__ref->joy_events_.push_back(Input::EventJoy{_joy, _event});
}

void Window::update_mouse_pos_and_buttons() {
    // Update buttons
    ImGuiIO& io = ImGui::GetIO();
    for (int i = 0; i < mouse_just_pressed.size(); i++)
    {
        // If a mouse press event came, always pass it as "mouse held this frame", so we don't miss click-release events that are shorter than 1 frame.
        io.MouseDown[i] = mouse_just_pressed[i] || glfwGetMouseButton(window, i) != 0;
        mouse_just_pressed[i] = false;
    }

    // Update mouse position
    const ImVec2 mouse_pos_backup = io.MousePos;
    io.MousePos = ImVec2(-FLT_MAX, -FLT_MAX);

    const bool focused = glfwGetWindowAttrib(window, GLFW_FOCUSED) != 0;

    if (focused)
    {
        if (io.WantSetMousePos)
        {
            glfwSetCursorPos(window, (double)mouse_pos_backup.x, (double)mouse_pos_backup.y);
        }
        else
        {
            double mouse_x, mouse_y;
            glfwGetCursorPos(window, &mouse_x, &mouse_y);
            io.MousePos = ImVec2((float)mouse_x, (float)mouse_y);
        }
    }
}

void Window::update_mouse_cursor() {
    ImGuiIO& io = ImGui::GetIO();
    if ((io.ConfigFlags & ImGuiConfigFlags_NoMouseCursorChange) || glfwGetInputMode(window, GLFW_CURSOR) == GLFW_CURSOR_DISABLED)
        return;

    ImGuiMouseCursor imgui_cursor = ImGui::GetMouseCursor();
    if (imgui_cursor == ImGuiMouseCursor_None || io.MouseDrawCursor)
    {
        // Hide OS mouse cursor if imgui is drawing it or if it wants no cursor
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
    }
    else
    {
        // Show OS mouse cursor
        // FIXME-PLATFORM: Unfocused windows seems to fail changing the mouse cursor with GLFW 3.2, but 3.3 works here.
        glfwSetCursor(window, mouse_cursors[imgui_cursor] ? mouse_cursors[imgui_cursor] : mouse_cursors[ImGuiMouseCursor_Arrow]);
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
    }
}

glm::ivec2 Window::get_resolution() {
    return ref->resolution;
}

float Window::get_aspect_ratio() {
    return ref->aspect_ratio;
}