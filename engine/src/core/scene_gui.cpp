/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <arc/core/scene.h>

#include <imgui/imgui.h>

#include <arc/core/app.h>
#include <arc/core/asset.h>
#include <arc/core/component.h>
#include <arc/core/entity.h>
#include <arc/gfx/material.h>
#include <arc/scene/mesh_renderer.h>
#include <arc/gfx/mesh.h>
#include <arc/particle/particle.h>

using namespace arc;

void Scene::gui_menu_entity() {
    auto active_scene = arc::SceneManager::get_active();
    if (!active_scene) {
        return;
    }

    id_t parent_entity = 0;
    if (selected_node_ && selected_node_->entity_) {
        parent_entity = selected_node_->entity_->get_id();
    }

    if (ImGui::MenuItem("Create Empty")) {
        add_entity(create_entity("Entity"), 0);
    }
    if (ImGui::MenuItem("Create Empty Child")) {
        add_entity(create_entity("Entity"), parent_entity);
    }
    ImGui::Separator();
    if (ImGui::BeginMenu("3D Object")) {
        if (ImGui::MenuItem("Cube")) {
            add_entity(create_3d_object("Cube", "cube.obj"), parent_entity);
        }
        if (ImGui::MenuItem("Sphere")) {
            add_entity(create_3d_object("Sphere", "sphere.obj"), parent_entity);
        }
        if (ImGui::MenuItem("Plane")) {
            add_entity(create_3d_object("Plane", "plane.obj"), parent_entity);
        }
        if (ImGui::MenuItem("Orb")) {
            add_entity(create_3d_object("Orb", "orb.obj"), parent_entity);
        }
        if (ImGui::MenuItem("Torus")) {
            add_entity(create_3d_object("Torus", "torus.obj"), parent_entity);
        }
        ImGui::EndMenu();
    }
    if (ImGui::BeginMenu("Effects")) {
        if (ImGui::MenuItem("Particle System")) {
            auto e = create_entity("ParticleSystem");
            e->add_component(std::make_shared<ParticleSystem>());
            add_entity(e, parent_entity);
        }
        ImGui::EndMenu();
    }
    if (ImGui::BeginMenu("Light")) {
        if (ImGui::MenuItem("Directional Light")) {
            add_entity(create_light("Directional Light", LightTypeDirectional), parent_entity);
        }
        if (ImGui::MenuItem("Point Light")) {
            add_entity(create_light("Point Light", LightTypePoint), parent_entity);
        }
        if (ImGui::MenuItem("Area Light")) {
            add_entity(create_light("Area Light", LightTypeArea), parent_entity);
        }
        if (ImGui::MenuItem("Spotlight")) {
            add_entity(create_light("Spotlight", LightTypeSpot), parent_entity);
        }
        ImGui::EndMenu();
    }
    if (ImGui::MenuItem("Camera")) {
        add_entity(create_camera("Camera"), 0);
    }
}

void Scene::gui_inspector_scene() {
    if (ImGui::CollapsingHeader("Hierarchy", ImGuiTreeNodeFlags_DefaultOpen)) {
        auto root = get_node(0);
        for (auto c : root->children_) {
            gui_node_walk(get_node(c));
        }
    }

    if (ImGui::CollapsingHeader("Inspector", ImGuiTreeNodeFlags_DefaultOpen)) {
        if (!selected_node_) {
            ImGui::Text("No node selected");

        } else {
            bool active = selected_node_->entity_->active_self;
            ImGui::Text("Selected node: %p", selected_node_);
            ImGui::Checkbox("Active", &selected_node_->entity_->active_self);
            if (active != selected_node_->entity_->active_self) {
                set_entity_active(selected_node_->entity_->get_id(), active);
            }

            for (auto &c : selected_node_->entity_->components) {
                if (ImGui::TreeNodeEx(c->get_class_name().c_str(), ImGuiTreeNodeFlags_DefaultOpen)) {
                    c->gui_inspector();
                    ImGui::TreePop();
                }
            }
        }
    }

    if (ImGui::CollapsingHeader("Debug")) {
        if (nodes_.size() <= 1) {
            ImGui::Text("Entity Count: 0");
        } else {
            ImGui::Text("Entity Count: %zd", nodes_.size()-1);
        }
        ImGui::Text("Entity Cache: %zd", e_cache_.size());
        ImGui::Text("Component Cache: %zd", c_cache_.size());
        ImGui::Text("Renderable Cache: %zd", r_cache_.size());
        ImGui::Text("Drawable Cache: %zd", d_cache_.size());
        ImGui::Text("Light Cache: %zd", l_cache_.size());
    }
}

void Scene::gui_node_walk(node *n) {
    if (!n) {
        return;
    }
    if (!n->entity_) {
        return;
    }
    ImGuiTreeNodeFlags node_flags = ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_OpenOnDoubleClick;

    if (n->children_.empty()) {
        node_flags |= ImGuiTreeNodeFlags_Leaf | ImGuiTreeNodeFlags_NoTreePushOnOpen;
        ImGui::TreeNodeEx(n->entity_->get_name().c_str(), node_flags);
        if (ImGui::IsItemClicked()) {
            selected_node_ = n;
        }
    } else {
        bool node_open = ImGui::TreeNodeEx(n->entity_->get_name().c_str(), node_flags);
        if (ImGui::IsItemClicked()) {
            selected_node_ = n;
        }
        if (node_open) {
            for (auto c : n->children_) {
                gui_node_walk(get_node(c));
            }
            ImGui::TreePop();

        }
    }
}

void Scene::gui_panel_inspector(bool* _p_open) {
    if (!selected_node_) {
        ImGui::Text("No node selected");

    } else {
        bool active = selected_node_->entity_->active_self;
        ImGui::Text("Selected node: %p", selected_node_);
        ImGui::Checkbox("Active", &selected_node_->entity_->active_self);
        if (active != selected_node_->entity_->active_self) {
            set_entity_active(selected_node_->entity_->get_id(), active);
        }

        for (auto &c : selected_node_->entity_->components) {
            if (ImGui::TreeNodeEx(c->get_class_name().c_str(), ImGuiTreeNodeFlags_DefaultOpen)) {
                c->gui_inspector();
                ImGui::TreePop();
            }
        }
    }
}

void Scene::gui_panel_hierarchy(bool* _p_open) {
    auto root = get_node(0);
    for (auto c : root->children_) {
        gui_node_walk(get_node(c));
    }
}

void Scene::gui_panel_renderer(bool* _p_open) {
    renderer_->gui_inspector();
}

void Scene::gui_panel_debug(bool* _p_open) {
    if (nodes_.size() <= 1) {
        ImGui::Text("Entity Count: 0");
    } else {
        ImGui::Text("Entity Count: %zd", nodes_.size()-1);
    }
    ImGui::Text("Entity Cache: %zd", e_cache_.size());
    ImGui::Text("Component Cache: %zd", c_cache_.size());
    ImGui::Text("Renderable Cache: %zd", r_cache_.size());
    ImGui::Text("Drawable Cache: %zd", d_cache_.size());
    ImGui::Text("Light Cache: %zd", l_cache_.size());
}
