/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#pragma once

#include <memory>
#include <string>
#include <list>

#include <arc/core/log.h>
#include <arc/core/system.h>
#include <arc/core/types.h>

namespace arc {
    class App {
    public:
        struct Properties {
            std::string name;
            std::string company_name;
            std::string version;
            load_fn pre_setup_func;
            load_fn post_setup_func;
        };

        explicit App(const Properties& _properties);

        void run();

        static void quit();
        static std::string get_name();
        static std::string get_company_name();
        static std::string get_version();

    private:
        void teardown();

        std::list<std::unique_ptr<System>> systems;
        std::unique_ptr<Log> log_;

        static std::string name;
        static std::string company_name;
        static std::string version;
        static bool running;
    };
}
