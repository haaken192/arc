/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#pragma once

#include <cstdint>
#include <set>
#include <string>
#include <vector>

#include <arc/core/system.h>
#include <arc/core/types.h>

namespace arc {
	class ObjectManager : public System {
	public:
		ObjectManager();
		~ObjectManager() override;

		static id_t assign();
		static void release(id_t id);
		static void release_all();

		static size_t count();

	private:
		std::set<id_t> objects;
		id_t next;

		static ObjectManager* ref;
	};

	class Object {
		friend class ObjectManager;

	public:
		Object();
		virtual ~Object() = default;

		void set_name(const std::string &name);

		std::string get_name() const;
		std::string get_class_name() const;
		id_t get_id() const;
		virtual void gui_inspector();

	protected:
		id_t id;
		std::string name;
		std::string class_name;
	};

	object_ptr new_object();
}
