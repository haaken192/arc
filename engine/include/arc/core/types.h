/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#pragma once

#include <memory>

namespace arc {
    typedef std::shared_ptr<class Component> component_ptr;
    typedef std::shared_ptr<class Entity> entity_ptr;
    typedef std::shared_ptr<class Object> object_ptr;
    typedef std::shared_ptr<class Transform> transform_ptr;
    typedef std::shared_ptr<class Scene> scene_ptr;
    typedef std::shared_ptr<class Material> material_ptr;
    typedef std::shared_ptr<class Mesh> mesh_ptr;
    typedef std::shared_ptr<class Shader> shader_ptr;
    typedef std::shared_ptr<class Texture> texture_ptr;
    typedef std::shared_ptr<class TextureCubemap> cubemap_ptr;
    typedef std::shared_ptr<class Resource> resource_ptr;
    typedef std::shared_ptr<class AttachmentGeneric> attachment_ptr;
    typedef std::shared_ptr<class Framebuffer> framebuffer_ptr;
    typedef std::shared_ptr<class GBuffer> gbuffer_ptr;
    typedef std::shared_ptr<class Renderable> renderable_ptr;
    typedef std::shared_ptr<class Drawable> drawable_ptr;
    typedef std::shared_ptr<class Skybox> skybox_ptr;
    typedef std::shared_ptr<class Renderer> renderer_ptr;
    typedef std::shared_ptr<class Light> light_ptr;

    typedef class Texture2D* texture2d_p;
    typedef Renderer* renderer_p;
    typedef Scene* scene_p;
    typedef Entity* entity_p;
    typedef Component* component_p;
    typedef Renderable* renderable_p;

    typedef int32_t id_t;

    typedef void (* load_fn)();

    enum LightType {
        LightTypeDirectional,
        LightTypePoint,
        LightTypeArea,
        LightTypeSpot
    };

    enum ImageFormat {
        ImageFormatHDR,
        ImageFormatJPG,
        ImageFormatPNG,
        ImageFormatUnknown,
    };

    enum MaterialTexture {
        MaterialTextureAttachment0,
        MaterialTextureAttachment1,
        MaterialTextureDepth,
        MaterialTextureEnvironment,
        MaterialTextureIrradiance,
        MaterialTextureAlbedo,
        MaterialTextureNormal,
        MaterialTextureMetallic,
    };

    enum TextureFormat {
        TextureFormatDefaultColor,
        TextureFormatDefaultHDRColor,
        TextureFormatDefaultDepth,
        TextureFormatR8,
        TextureFormatRG8,
        TextureFormatRGB8,
        TextureFormatRGBA8,
        TextureFormatR16,
        TextureFormatRG16,
        TextureFormatRGB16,
        TextureFormatRGBA16,
        TextureFormatRGBA16UI,
        TextureFormatR32,
        TextureFormatRG32,
        TextureFormatRGB32,
        TextureFormatRGBA32,
        TextureFormatRGB32UI,
        TextureFormatRGBA32UI,
        TextureFormatDepth16,
        TextureFormatDepth24,
        TextureFormatDepth24Stencil8,
        TextureFormatStencil8,
    };

    enum RenderMode {
        RenderModeForward,
        RenderModeDeferred,
    };

    enum Message {
        MessageActivate,
        MessageStart,
        MessageAwake,
        MessageUpdate,
        MessageLateUpdate,
        MessageFixedUpdate,
        MessageTransform,
        MessageGUIRender,
        MessageSGUpdate,
    };
}
