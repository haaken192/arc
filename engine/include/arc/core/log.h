/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#pragma once

#include <cstdarg>
#include <shared_mutex>
#include <string>

#include <imgui/imgui.h>
#include <spdlog/sinks/base_sink.h>
#include <spdlog/spdlog.h>

namespace arc {
    enum LogLevel {
        trace = spdlog::level::trace,
        debug = spdlog::level::debug,
        info = spdlog::level::info,
        warn = spdlog::level::warn,
        error = spdlog::level::err,
        critical = spdlog::level::critical,
        off = spdlog::level::off,
    };

    class GUILogSink : public spdlog::sinks::base_sink<std::mutex> {
    public:
        void clear();
        void toggle_wrap();
        void toggle_scroll_lock();
        void draw();

    protected:
        void sink_it_(const spdlog::details::log_msg& _msg) override;
        void flush_() override;

    private:
        struct LogRecord {
            std::string message_;
        };

        std::vector<LogRecord> records_;
        mutable std::shared_timed_mutex records_mutex_;
        ImGuiTextFilter display_filter_;

        bool scroll_to_bottom_{true};
        bool wrap_{false};
        bool scroll_lock_{false};
    };

    class Log {
        friend class App;

    public:
        Log();
        ~Log();

        void set_log_level(LogLevel _level);
        LogLevel get_log_level() const;

        static void show_gui();

        std::shared_ptr<spdlog::logger> logger_;
        std::shared_ptr<GUILogSink> gui_sink_;
    private:
        LogLevel level_;
    };


    spdlog::logger* get_logger();
    LogLevel get_log_level();
    void set_log_level(LogLevel _level);

#define LOG_LEVEL_CMP(LOGGER, LEVEL) \
    (static_cast<spdlog::level::level_enum>(arc::LogLevel::LEVEL) >= LOGGER->level())

#define LOG_MSG(LOGGER, LEVEL, ...) \
    if (LOG_LEVEL_CMP(LOGGER, LEVEL)) { LOGGER->LEVEL(__VA_ARGS__); }

#define LOG_TRACE(...) \
    LOG_MSG(arc::get_logger(), trace, __VA_ARGS__)

#define LOG_DEBUG(...) \
    LOG_MSG(arc::get_logger(), debug, __VA_ARGS__)

#define LOG_INFO(...) \
    LOG_MSG(arc::get_logger(), info, __VA_ARGS__)

#define LOG_WARN(...) \
    LOG_MSG(arc::get_logger(), warn, __VA_ARGS__)

#define LOG_ERROR(...) \
    LOG_MSG(arc::get_logger(), error, __VA_ARGS__)

#define LOG_CRITICAL(...) \
    LOG_MSG(arc::get_logger(), critical, __VA_ARGS__)
}
