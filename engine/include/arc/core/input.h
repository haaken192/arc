/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#pragma once

#include <arc/core/system.h>

#include <vector>

#include <glm/glm.hpp>
#include <GLFW/glfw3.h>

namespace arc {
    class Input : public System {
        friend class App;
        friend class Window;

    public:
        struct EventKey {
            int scancode_;
            int key_;
            int action_;
            int mod_;
        };

        struct EventMouseButton {
            int button_;
            int action_;
            int mod_;
        };

        struct EventJoy {
            int joystick_;
            int event_;
        };

        Input();
        ~Input() override;

        void reset();
        void defer_gui_events();

        static bool key_down(int _key);
        static bool key_up(int _key);
        static bool key_pressed(int _key);
        static bool mouse_down(int _button);
        static bool mouse_up(int _button);
        static bool mouse_wheel();
        static bool mouse_moved();
        static bool mouse_pressed();
        static bool window_resized();
        static bool has_events();
        static double mouse_wheel_x();
        static double mouse_wheel_y();
        static glm::ivec2 mouse_position();

    private:
        std::vector<EventMouseButton> mouse_button_events_;
        std::vector<EventKey> key_events_;
        std::vector<EventJoy> joy_events_;
        glm::dvec2 scroll_axis_;
        glm::ivec2 cursor_position_;
        bool scroll_moved_;
        bool cursor_moved_;
        bool window_resized_;
        bool has_events_;

        static Input* __ref;
    };
}