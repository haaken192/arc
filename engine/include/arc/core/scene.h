/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#pragma once

#include <map>
#include <stack>
#include <string>
#include <vector>

#include <arc/core/system.h>
#include <arc/core/entity.h>
#include <arc/core/types.h>

namespace arc {
    class SceneManager : public System {
        friend class App;

    public:
        SceneManager();
        ~SceneManager() override;

        static void add(scene_ptr scene);
        static void load(const std::string &name);
        static void purge_push(const std::string &name);
        static void replace(const std::string &name);
        static void push(const std::string &name);
        static void pop();
        static void remove_all();
        static Scene* get_active();
        static std::string get_active_name();
    private:
        void update();
        void fixed_update();
        void display();

        std::map<std::string, scene_ptr> scenes_;
        std::stack<std::string> active_;

        static SceneManager* ref__;
    };

    class Scene : public std::enable_shared_from_this<Scene> {
        friend class SceneManager;
    public:
        explicit Scene(const std::string &name);

        std::string get_name() const;
        bool is_loaded() const;
        void load();
        void on_activate();
        void on_deactivate();
        void add_entity(entity_ptr _entity, id_t _parent);
        void move_entity(id_t id, id_t parent);
        void remove_entity(id_t id);
        void set_entity_active(id_t id, bool active);
        bool has_entity(id_t id) const;
        bool entity_has_child(id_t id, id_t child) const;
        entity_ptr entity_by_name(const std::string &name) const;
        entity_ptr entity_parent(id_t id) const;
        entity_ptr entity_at_node(id_t id) const;
        std::vector<entity_ptr> entity_children(id_t id) const;
        std::vector<entity_ptr> entity_all_children(id_t id) const;
        std::vector<component_ptr> entity_components_in_parent(id_t id) const;
        std::vector<component_ptr> entity_components_in_children(id_t id) const;
        const renderer_p get_renderer() const;
        Skybox* get_skybox() const;
        void set_skybox(skybox_ptr _skybox);

        void gui_panel_inspector(bool* _p_open);
        void gui_panel_hierarchy(bool* _p_open);
        void gui_panel_debug(bool* _p_open);
        void gui_panel_renderer(bool* _p_open);
        void gui_menu_entity();
        void gui_inspector_scene();

        load_fn load_func_;
        load_fn update_func_;
        load_fn display_func_;

    protected:
        struct node {
            node(entity_ptr _entity, id_t _parent, bool _active)
            : entity_(std::move(_entity))
            , parent_(_parent)
            , active_(_active) {}

            entity_ptr entity_;
            id_t parent_;
            std::vector<id_t> children_;
            bool active_;
        };

        void update();
        void fixed_update();
        void display();
        void update_caches();
        void update_tree(node *n, bool active);
        void node_remove_child(node *n, id_t child);
        void send_message(Message _m);
        std::vector<id_t> dfs(id_t id, bool include_inactive);
        std::vector<entity_ptr> node_all_child_entities(node *n) const;
        std::vector<entity_ptr> node_all_parent_entities(node *n) const;
        node* get_node(id_t id) const;

        /* GUI */
        void gui_node_walk(node *n);
        void gui_menu_main();

        std::map<id_t, std::unique_ptr<node>> nodes_;
        std::vector<entity_ptr> e_cache_;
        std::vector<component_ptr> c_cache_;
        std::vector<renderable_ptr> r_cache_;
        std::vector<drawable_ptr> d_cache_;
        std::vector<light_ptr> l_cache_;
        skybox_ptr skybox_;
        renderer_ptr renderer_;
        std::string name_;
        node *selected_node_;
        bool loaded_;
        bool started_;
        bool dirty_;
    };
}
