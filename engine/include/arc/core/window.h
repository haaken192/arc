/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#pragma once

#include <array>
#include <memory>

#include <glm/glm.hpp>
#include <gl3w/GL/gl3w.h>
#include <GLFW/glfw3.h>
#include <imgui/imgui.h>

#include "system.h"

namespace arc {
    class Window : public System {
        friend class App;

    public:
        Window();
        ~Window() override;

        void handle_events();
        void begin_frame();
        void end_frame();
        void set_size(const glm::ivec2 &size);

        static glm::ivec2 get_resolution();
        static float get_aspect_ratio();

    private:
        void init_gui();
        void draw_gui();
        void update_mouse_pos_and_buttons();
        void update_mouse_cursor();

        static void cb_char(GLFWwindow*, unsigned int c);
        static void cb_key(GLFWwindow*, int key, int, int action, int mods);
        static void cb_scroll(GLFWwindow*, double xoffset, double yoffset);
        static void cb_mouse_button(GLFWwindow*, int button, int action, int /*mods*/);
        static void cb_window_resize(GLFWwindow*, int width, int height);
        static void cb_mouse_move(GLFWwindow*, double _x, double _y);
        static void cb_joystick_event(int _joy, int _event);

        glm::mat4 ortho_matrix;
        glm::ivec2 resolution;
        glm::ivec2 mouse_position;
        float aspect_ratio;
        bool mouse_relative;
        GLFWwindow* window;
        std::array<bool, 5> mouse_just_pressed;
        std::array<GLFWcursor*, ImGuiMouseCursor_COUNT> mouse_cursors;
        std::unique_ptr<class Shader> gui_shader;
        GLuint gui_vao;
        GLuint gui_vbo;
        GLuint gui_ibo;
        GLuint gui_font_texture;
        bool show_demo_gui;

        static Window* ref;
    };
}