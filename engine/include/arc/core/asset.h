/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#pragma once

#include <arc/core/system.h>

#include <map>
#include <memory>
#include <vector>

#include <arc/core/types.h>

namespace arc {
    class GenericAssetHandler {
    public:
        explicit GenericAssetHandler(std::string _type)
        : type_(std::move(_type)) {}

        virtual void load(const resource_ptr& _resource) = 0;
        virtual void clear() = 0;
        virtual void gui_inspector() = 0;
        virtual size_t count() const = 0;

        std::string get_type() const {
            return type_;
        }

    protected:
        std::string type_;
    };

    template<typename T>
    class AssetHandler : public GenericAssetHandler {
    public:
        explicit AssetHandler(std::string _type)
        : GenericAssetHandler(std::move(_type)) {}

        T* get_asset_ptr(const std::string& _name) const {
            auto itr = items_.find(_name);
            if (itr == items_.cend())
                throw std::invalid_argument("Unable to find asset: " + _name);

            return itr->second.get();
        }

        std::shared_ptr<T> get_asset(const std::string& _name) const {
            auto itr = items_.find(_name);
            if (itr == items_.cend())
                throw std::invalid_argument("Unable to find asset: " + _name);

            return itr->second;
        }

        void add_asset(const std::string& _name, std::shared_ptr<T> _asset, bool _force_add = false) {
            if (has_asset(_name) && !_force_add)
                throw std::invalid_argument("Unable to add asset: " + _name + ", it has already been added");

            items_[_name] = _asset;
        }

        bool has_asset(const std::string& _name) const {
            return items_.find(_name) != items_.cend();
        }

        size_t count() const override {
            return items_.size();
        }

        void clear() override {
            items_.clear();
        }

    protected:
        std::map<std::string, std::shared_ptr<T>> items_;
    };

    class AssetManager : public System {
    public:
        struct AssetQueue {
            AssetQueue(std::string _type, std::string _filename)
            : type_(std::move(_type))
            , filename_(std::move(_filename)) {}
            std::string type_;
            std::string filename_;
        };

        struct Metrics {
            uint32_t total_;
            uint32_t processed_;
            std::string current_asset_;

            float progress() const;
        };

        AssetManager();
        ~AssetManager() override;

        static void register_handler(std::shared_ptr<GenericAssetHandler> handler);
        static void add_manifest(const std::string& filename);
        static void process(bool unbounded = false);
        static void reset_metrics();
        static void gui_inspector();
        static size_t size();
        static size_t size_by_type(const std::string& name);
        static bool has_handler(const std::string& handler);
        static bool done();
        static Metrics get_metrics();
        static void show_gui();

        template<typename T>
        static std::shared_ptr<T> get_asset_ptr(const std::string& _name) {
            for (auto const &c : ref__->handlers_) {
                if (auto h = std::dynamic_pointer_cast<AssetHandler<T>>(c.second)) {
                    return h->get_asset(_name);
                }
            }

            throw::std::invalid_argument("Could not find a AssetHandler for Asset: " + _name);
        }

        template<typename T>
        static T* get_asset(const std::string& _name) {
            for (auto const &c : ref__->handlers_) {
                if (auto h = std::dynamic_pointer_cast<AssetHandler<T>>(c.second)) {
                    return h->get_asset_ptr(_name);
                }
            }

            throw::std::invalid_argument("Could not find a AssetHandler for Asset: " + _name);
        }

        template<typename T>
        static T* add_asset(const std::string& _name, std::shared_ptr<T> _asset, bool _force_add = false) {
            for (auto const &c : ref__->handlers_) {
                if (auto h = std::dynamic_pointer_cast<AssetHandler<T>>(c.second)) {
                    add_asset(_name, _asset, _force_add);
                }
            }

            throw::std::invalid_argument("Could not find a AssetHandler for Asset: " + _name);
        }

    private:
        std::map<std::string, std::shared_ptr<class GenericAssetHandler>> handlers_;
        std::vector<AssetQueue> load_queue_;
        Metrics metrics_;

        static AssetManager* ref__;
    };
}