/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

#include "arc/core/component.h"

namespace arc {
	class Transform : virtual public Component {
	public:
		Transform();

		glm::mat4 get_model_matrix() const;
		glm::mat4 get_active_matrix() const;
		glm::fquat get_rotation() const;
		glm::vec3 get_position() const;
		glm::vec3 get_scale() const;
		void gui_inspector() override;
		virtual void set_rotation(const glm::fquat &rotation, bool propagate);
		virtual void set_position(const glm::vec3 &position, bool propagate);
		virtual void set_scale(const glm::vec3 &scale, bool propagate);

		virtual void recompute(bool propagate);

	protected:
		glm::mat4 model_matrix_;
		glm::mat4 active_matrix_;
		glm::fquat rotation_;
		glm::vec3 position_;
		glm::vec3 scale_;
	};
}
