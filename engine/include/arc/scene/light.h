/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#pragma once

#include <arc/core/component.h>

#include <arc/core/color.h>

namespace arc {
    class Light : public Component {
    public:
        Light();
        explicit Light(LightType _type);
        Light(LightType _type, Color _color);

        void gui_inspector() override;

        void set_color(Color _color);
        void set_area_size(glm::vec2 _area_size);
        void set_type(LightType _type);
        void set_bounce_intensity(float _bounce_intensity);
        void set_color_temperature(float _color_temperature);
        void set_intensity(float _intensity);
        void set_range(float _range);
        void set_spot_angle(float _spot_angle);

        Color get_color() const;
        glm::vec2 get_area_size() const;
        LightType get_type() const;
        float get_bounce_intensity() const;
        float get_color_temperature() const;
        float get_intensity() const;
        float get_range() const;
        float get_spot_angle() const;

    private:
        Color color_;
        glm::vec2 area_size_;
        LightType type_;
        float bounce_intensity_;
        float color_temperature_;
        float intensity_;
        float range_;
        float spot_angle_;
    };

    const char* light_type_to_str(LightType _type);
}
