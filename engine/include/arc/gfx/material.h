/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#pragma once

#include <gl3w/GL/gl3w.h>
#include <glm/glm.hpp>

#include <arc/core/object.h>
#include <arc/gfx/shader.h>

namespace arc {
    class Material : public Object {
    public:
        explicit Material(shader_ptr _shader);

        void set_shader(shader_ptr shader);
        void set_texture(texture_ptr texture, MaterialTexture id);
        void set_texture(texture_ptr texture, uint8_t id);
        void set_property(const std::string &name, const ShaderProperty &value);
        Shader* get_shader() const;
        Texture* get_texture(MaterialTexture id) const;
        Texture* get_texture(uint8_t id) const;
        bool supports_deferred() const;
        void bind();
        void unbind();
        void gui_inspector() override;

    private:
        shader_ptr shader_;
        std::map<std::string, ShaderProperty> shader_properties_;
        std::map<GLuint, texture_ptr> textures_;

        std::shared_ptr<class TextureColor> albedo_;
        std::shared_ptr<class TextureColor> metallic_;
        std::shared_ptr<class TextureColor> roughness_;
    };
}
