/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#pragma once

#include <cstdint>
#include <map>
#include <string>
#include <vector>

#include <boost/variant/variant.hpp>
#include <boost/variant/static_visitor.hpp>
#include <gl3w/GL/gl3w.h>
#include <glm/glm.hpp>

#include <arc/core/object.h>
#include <arc/core/asset.h>

namespace arc {
    typedef boost::variant<bool, int, float, GLuint, glm::vec2, glm::vec3, glm::vec4, glm::mat2, glm::mat3, glm::mat4> ShaderProperty;

    static const char* SHADER_PROPERTY_TYPES[] = {
        "Bool",
        "Int",
        "Float",
        "Uint",
        "Vec2",
        "Vec3",
        "Vec4",
        "Mat2",
        "Mat3",
        "Mat4"
    };

    enum ShaderComponent {
        ShaderComponentVertex = GL_VERTEX_SHADER,
        ShaderComponentGeometry = GL_GEOMETRY_SHADER,
        ShaderComponentFragment = GL_FRAGMENT_SHADER,
        ShaderComponentCompute = GL_COMPUTE_SHADER,
        ShaderComponentTessControl = GL_TESS_CONTROL_SHADER,
        ShaderComponentTessEvaluation = GL_TESS_EVALUATION_SHADER
    };

    const char* shader_component_to_str(ShaderComponent _component);

    class shader_property_gui_visitor : public boost::static_visitor<> {
    public:
        void operator()(bool& _value) const;
        void operator()(int& _value) const;
        void operator()(float& _value) const;
        void operator()(GLuint& _value) const;
        void operator()(glm::vec2& _value) const;
        void operator()(glm::vec3& _value) const;
        void operator()(glm::vec4& _value) const;
        void operator()(glm::mat2& _value) const;
        void operator()(glm::mat3& _value) const;
        void operator()(glm::mat4& _value) const;
    };

    class shader_property_name_visitor : public boost::static_visitor<const char*> {
    public:
        const char* operator()(bool) const;
        const char* operator()(int) const;
        const char* operator()(float) const;
        const char* operator()(GLuint) const;
        const char* operator()(glm::vec2) const;
        const char* operator()(glm::vec3) const;
        const char* operator()(glm::vec4) const;
        const char* operator()(glm::mat2) const;
        const char* operator()(glm::mat3) const;
        const char* operator()(glm::mat4) const;
    };

    class shader_property_visitor : public boost::static_visitor<>
    {
    public:
        explicit shader_property_visitor(const GLuint &program_id, const char *uniform_name);

        void operator()(const bool &value) const;
        void operator()(const int &value) const;
        void operator()(const float &value) const;
        void operator()(const GLuint &value) const;
        void operator()(const glm::vec2 &value) const;
        void operator()(const glm::vec3 &value) const;
        void operator()(const glm::vec4 &value) const;
        void operator()(const glm::mat2 &value) const;
        void operator()(const glm::mat3 &value) const;
        void operator()(const glm::mat4 &value) const;

    private:
        explicit shader_property_visitor() = default;

        GLuint program_id;
        const char *uniform_name;
    };

    class Shader : public Object {
        friend class ShaderHandler;

    public:
        explicit Shader(const std::string &name, bool _deferred = false);
        ~Shader() override;

        void bind() const;
        void unbind() const;
        void add_data(const std::vector<char>& data);
        void add_data(const std::string& data);
        bool load(const resource_ptr& _resource);
        void clear_data();
        bool compile();
        bool recompile();
        void set_subroutine(const ShaderComponent &component_type, const std::string &subroutine_name) const;
        void set_uniform(const std::string &_uniform_name, const ShaderProperty &_value) const;
        bool supports_deferred() const;
        void gui_inspector() override;

        uint32_t get_reference() const;

    private:
        static void destroy_component(const GLuint &program_id, const GLuint &component_id);
        static bool validate_component(const GLuint &component_id);
        static bool validate_program(const GLuint &program_id);
        static bool contains_shader_type(ShaderComponent type, const std::string &data);
        static GLuint load_component(const GLuint &program_id, ShaderComponent type, const std::string &data);
        static void destory_shader_parts(const GLuint &program_id, std::map<ShaderComponent, GLuint> &components);

        GLuint reference;
        std::string data;
        std::map<ShaderComponent, GLuint> components;
        resource_ptr resource_;
        bool deferred_;
    };

    class ShaderHandler : public AssetHandler<Shader> {
    public:
        ShaderHandler();

        void load(const resource_ptr& _resource) override;
        void gui_inspector() override;
    };
}