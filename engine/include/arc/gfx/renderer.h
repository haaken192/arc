/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#pragma once

#include <vector>

#include <glm/glm.hpp>

#include <arc/core/system.h>
#include <arc/core/types.h>
#include <arc/gfx/ssao.h>
#include <arc/gfx/tonemap.h>

namespace arc {
    class Renderable {
    public:
        virtual float get_near_clip() const = 0;
        virtual float get_far_clip() const = 0;
        virtual glm::vec3 get_position() const = 0;
        virtual glm::mat3 get_normal_matrix() const = 0;
        virtual glm::mat4 get_view_matrix() const = 0;
        virtual glm::mat4 get_projection_matrix() const = 0;
    };

    class Drawable {
    public:
        virtual void draw(const Renderable* _renderable) = 0;
        virtual void draw_shader(const Renderable* _renderable, const Shader* _shader) = 0;
        virtual bool is_deferrable() const = 0;
    };

    class Renderer {
    public:
        Renderer();

        void render(
            const Renderable* _renderable,
            const std::vector<drawable_ptr>& _drawables,
            const std::vector<light_ptr>& _lights);
        void resize();
        void gui_inspector();

        RenderMode get_active_mode() const;

    private:
        framebuffer_ptr fbo_;
        shader_ptr shader_;
        shader_ptr skybox_shader_;
        mesh_ptr back_mesh_;

        std::shared_ptr<class AttachmentTexture2D> tex_gbuffer_0_;
        std::shared_ptr<class AttachmentTexture2D> tex_gbuffer_1_;
        std::shared_ptr<class AttachmentTexture2D> tex_hdr_target_;
        std::shared_ptr<class AttachmentTexture2D> tex_ldr_target_;
        std::shared_ptr<class AttachmentTexture2D> tex_depth_;
        std::shared_ptr<class Texture2D> tex_brdf_lut_;
        std::unique_ptr<SSAO> ssao_;
        std::unique_ptr<Tonemap> tonemap_;
        RenderMode active_mode_;
    };
}
