/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#pragma once

#include <vector>

#include <glm/glm.hpp>

#include <arc/core/types.h>

namespace arc {
    class SSAO {
        friend class Renderer;
        
    public:
        SSAO();

        void compute(const texture_ptr& _gbuffer_0, const texture_ptr& _gbuffer_1, const texture_ptr& _depth, const Renderable* _renderable);
        void gui_inspector();

    private:
        void generate_textures();

        framebuffer_ptr fbo_;
        shader_ptr ssao_shader_;
        shader_ptr blur_shader_;
        std::shared_ptr<class AttachmentTexture2D> ssao_attachment_;
        std::shared_ptr<class AttachmentTexture2D> ssao_blur_attachment_;
        texture_ptr noise_texture_;
        mesh_ptr mesh_;
        std::vector<glm::vec3> ssao_kernel_;
        float radius_;
        float bias_;
        uint8_t kernel_samples_;
        bool enabled_;
    };
}