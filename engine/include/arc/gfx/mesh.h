/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#pragma once

#include <glm/glm.hpp>

#include <arc/core/object.h>
#include <arc/core/asset.h>

namespace arc {
    struct Vertex {
        glm::vec3 position;
        glm::vec3 normal;
        glm::vec2 uv;

        bool operator==(const Vertex& _other) const {
            return position == _other.position && normal == _other.normal && uv == _other.uv;
        }
    };

    class Mesh : public Object {
    public:
        enum ObjFaceType
        {
            ObjFaceTypeUnknown,
            ObjFaceTypeV,
            ObjFaceTypeVT,
            ObjFaceTypeVN,
            ObjFaceTypeVTN
        };

        Mesh();
        ~Mesh() override;

        void set_positions(std::vector<glm::vec3> positions);
        void set_normals(std::vector<glm::vec3> normals);
        void set_uvs(std::vector<glm::vec2> uvs);
        void upload();
        void bind() const;
        void unbind() const;
        void draw() const;
        void clear();
        bool load(const resource_ptr& _resource);

        std::vector<glm::vec3> get_positions() const;
        std::vector<glm::vec3> get_normals() const;
        std::vector<glm::vec2> get_uvs() const;

    private:
        std::vector<glm::vec3> positions;
        std::vector<glm::vec3> normals;
        std::vector<glm::vec2> uvs;

        uint32_t vao;
        uint32_t vbo;
        uint32_t ibo_;
        int32_t verts;
        int32_t indices_;
    };

    class MeshHandler : public AssetHandler<Mesh> {
    public:
        MeshHandler();

        void load(const resource_ptr& _resource) override;
        void gui_inspector() override;
    };

    mesh_ptr create_mesh_quad();
    mesh_ptr create_mesh_quadback();
}