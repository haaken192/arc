/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#pragma once

#include <arc/core/object.h>

#include <map>
#include <stack>
#include <vector>

#include <glm/glm.hpp>
#include <gl3w/GL/gl3w.h>

namespace arc {
    class Framebuffer : virtual public Object, public std::enable_shared_from_this<Framebuffer> {
    public:
        Framebuffer();
        explicit Framebuffer(glm::ivec2 _size);
        ~Framebuffer() override;

        bool allocate();
        void raw_bind() const;
        void raw_unbind() const;
        void bind();
        void unbind();
        void update_attachments();
        void clear_buffers() const;
        void clear_buffer_flags(uint32_t _flags) const;
        bool validate() const;
        glm::ivec2 get_size() const;
        uint32_t get_reference() const;
        bool has_attachment(uint32_t _location) const;
        void set_size(glm::ivec2 _size);
        void set_draw_buffers(std::vector<uint32_t> _buffers);
        void apply_draw_buffers(std::vector<uint32_t> _buffers);
        void set_attachment(uint32_t _location, attachment_ptr _attachment);
        void remove_all_attachments();
        void remove_attachment(uint32_t _location);
        attachment_ptr get_generic_attachment(uint32_t _location) const;
        AttachmentGeneric* get_generic_attachment_p(uint32_t _location) const;

        static void bind_current();
        static framebuffer_ptr current();
        static void blit_framebuffers(const framebuffer_ptr& _in, const framebuffer_ptr& _out, GLenum _location);

        template<typename T>
        std::shared_ptr<T> get_attachment(uint32_t _location) const {
            return std::dynamic_pointer_cast<T>(get_generic_attachment(_location));
        }

        template<typename T>
        T* get_attachment_p(uint32_t _location) const {
            return dynamic_cast<T*>(get_generic_attachment_p(_location));
        }

    protected:
        static void pop();
        static void push(framebuffer_ptr fbo);

        std::map<uint32_t, attachment_ptr> attachments_;
        std::vector<uint32_t> draw_buffers_;
        glm::ivec2 size_;
        uint32_t reference_;
        bool bound_;

        static std::stack<framebuffer_ptr> framebuffer_stack;
    };

    class GBuffer : public Framebuffer {
    public:
        explicit GBuffer(glm::ivec2 _size, std::shared_ptr<class AttachmentTexture2D> _depth, bool _hdr);

        bool is_hdr() const;

        texture_ptr get_attachment_0() const;
        const class Texture2D* get_attachment_0_p() const;
        texture_ptr get_attachment_1() const;
        const class Texture2D* get_attachment_1_p() const;
        texture_ptr get_attachment_2() const;
        const class Texture2D* get_attachment_2_p() const;
        texture_ptr get_attachment_depth() const;
        const class Texture2D* get_attachment_depth_p() const;

    private:
        bool hdr_;
    };
}
