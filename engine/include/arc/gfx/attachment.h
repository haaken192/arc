/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#pragma once

#include <memory>

#include <gl3w/GL/gl3w.h>
#include <glm/glm.hpp>

#include <arc/core/object.h>

namespace arc {
    class AttachmentGeneric {
    public:
        virtual void attach(GLenum _location) = 0;
        virtual void set_size(glm::ivec2 _size) = 0;
    };

    template<typename T>
    class Attachment : virtual public AttachmentGeneric {
    public:
        explicit Attachment(std::shared_ptr<T> _object)
        : object_(_object) {}

        std::shared_ptr<T> get_object() const {
            return object_;
        }
    protected:
        std::shared_ptr<T> object_;
    };

    class AttachmentRenderbuffer : public Attachment<class Renderbuffer> {
    public:
        explicit AttachmentRenderbuffer(std::shared_ptr<class Renderbuffer> _renderbuffer);
        AttachmentRenderbuffer(GLenum _format, glm::ivec2 _size);

        void attach(GLenum _location) override;
        void set_size(glm::ivec2 _size) override;
    };

    class AttachmentTexture2D : public Attachment<class Texture2D> {
    public:
        explicit AttachmentTexture2D(std::shared_ptr<class Texture2D> _texture);
        AttachmentTexture2D(TextureFormat _format, glm::ivec2 _size);

        void attach(GLenum _location) override;
        void set_size(glm::ivec2 _size) override;
    };
}

