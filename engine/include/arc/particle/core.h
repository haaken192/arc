/*
Copyright (c) 2018 HaakenLabs

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#pragma once

#include <arc/particle/buffer.h>
#include <arc/core/color.h>

namespace arc {
    class ParticleSystem;

    namespace particle {
        class Core {
            friend class arc::ParticleSystem;

        public:
            Core(uint32_t _max_particles);

            void set_max_particles(uint32_t _max_particles);
            void sync_counts();
            void bind_buffer() const;
            void unbind_buffer() const;

            uint32_t get_max_particles() const;
            uint32_t get_particle_count() const;

            Color start_color;
            float duration;
            float start_delay;
            float start_lifetime;
            float start_speed;
            float start_size;
            float playback_speed;
            uint32_t random_seed;
            bool looping;

        private:
            std::unique_ptr<Buffer> buffer_;
            shader_ptr lifecycle_shader_;
            shader_ptr simulate_shader_;
            uint32_t alive_;
            uint32_t dead_;
            uint32_t emit_;
            uint32_t max_particles_;
        };
    }
}
